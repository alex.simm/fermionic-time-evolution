#include "test_utils.h"
#include "../src/utils/StorageFile.h"
#include "../src/utils.h"
#include <armadillo>
#include <filesystem>

using namespace std;
using namespace arma;

TEST_CASE("Storage file class tests") {

    SECTION("Read and write test") {
        arma_rng::set_seed_random();
        const string filename = std::filesystem::temp_directory_path() / "testfile.abc";
        const uint size = 100;

        vector<vector<double>> data;
        vector<string> names;
        for (uint i = 0; i < 4; ++i) {
            data.push_back(Utils::convert<vec, double>(randu(size)));
            names.push_back(to_string(i));
        }

        StorageFile file(filename);
        file.setColumns(data, names);
        file.save();

        StorageFile file2(filename);
        file2.load();
        CHECK(file2.getColumnNames() == names);
        CHECK(file2.getColumns().size() == data.size());
        for (int i = 0; i < data.size(); ++i) {
            testVectorApprox(file2.getColumn(i), data[i]);
        }
        file2.deleteFile();
    }
}
