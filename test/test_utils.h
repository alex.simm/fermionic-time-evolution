#ifndef IMPURITY_MODEL_TESTS_H
#define IMPURITY_MODEL_TESTS_H

#include "catch.hpp"
#include <vector>
#include <armadillo>

//TODO: move into class
void testComplexApprox(std::complex<double> c1, std::complex<double> c2);

void testVectorApprox(std::vector<double> v1, std::vector<double> v2);

void testVectorApprox(arma::vec v1, arma::vec v2);

void testVectorApprox(arma::cx_vec v1, arma::cx_vec v2);

template<class T>
void testArmaApprox(T obj1, T obj2) {
    CHECK(approx_equal(obj1, obj2, "absdiff", 1e-5));
}

class TestUtils {
public:
    static arma::vec createRandomVector(uint n, bool normalised = false);

    static arma::cx_vec createRandomCplxVector(uint n, bool normalised = false);
};

#endif //IMPURITY_MODEL_TESTS_H
