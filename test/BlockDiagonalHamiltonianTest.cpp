#include <iostream>
#include "test_utils.h"
#include "../src/HamiltonianBlock.h"
#include "../src/utils.h"
#include "../src/hamiltonians/OnSiteEnergy.h"
#include "DiagonalHamiltonianTerm.h"
#include "../src/BlockDiagonalHamiltonian.h"

using namespace std;
using namespace arma;

//TODO
TEST_CASE("Block diagonal Hamiltonian class tests") {

    SECTION("Number of blocks, basis size, particle number per block") {
        for (uint N = 1; N < 12; N++) {
            BlockDiagonalHamiltonian H(N, {});
            CHECK(H.getBasis().getN() == N);
            CHECK(H.getBlocks().size() == N + 1);
            CHECK(H.getBasis().dim() == (ulong) pow(2, N));

            const vector<HamiltonianBlock *> blocks = H.getBlocks();
            for (int i = 0; i < N + 1; ++i) {
                CHECK(blocks[i] == H.getBlock(i));
                CHECK(blocks[i]->getBasis().getQ() == i);
            }
        }
    }
}
