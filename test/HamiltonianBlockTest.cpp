#include <iostream>
#include "test_utils.h"
#include "../src/HamiltonianBlock.h"
#include "../src/utils.h"
#include "../src/hamiltonians/OnSiteEnergy.h"
#include "DiagonalHamiltonianTerm.h"

using namespace std;
using namespace arma;

//TODO: add more tests for impurity
TEST_CASE("Hamiltonian block class tests") {

    SECTION("Diagonalisation of Hamiltonian") {
        HamiltonianBlock H(8, 4, {});
        CHECK_FALSE(H.isDiagonalised());
        CHECK_THROWS(H.getEigenvalues().size());
        CHECK_THROWS(H.getEigenvectors().size());

        H.diagonalise();
        CHECK(H.isDiagonalised());
        CHECK(H.getEigenvalues().n_rows == H.getBasis().dim());
    }

    SECTION("Partition function") {
        DiagonalHamiltonianTerm diagonal({1.0, 2.0, 3.0, 4.0});
        HamiltonianBlock H(4, 1, {&diagonal});
        H.diagonalise();
        CHECK(H.partitionFunction(1) == Approx(0.57131743));
        CHECK(H.partitionFunction(0.5) == Approx(1.332875544));

        diagonal = DiagonalHamiltonianTerm({-2.0, -1.0, 0.0, 1.0, 2.0});
        HamiltonianBlock H2(5, 1, {&diagonal});
        H2.diagonalise();
        CHECK(H2.partitionFunction(1) == Approx(11.61055265179775));
        CHECK(H2.partitionFunction(0.5) == Approx(6.34141320004324));
    }

    SECTION("Average energy") {
        DiagonalHamiltonianTerm diagonal(linspace(1.0, 4.0, 4));
        HamiltonianBlock H(4, 1, {&diagonal});
        H.diagonalise();
        CHECK(H.averageEnergy(1) == Approx(1.50734726541));
        CHECK(H.averageEnergy(0.5) == Approx(1.9154235115381));

        diagonal = DiagonalHamiltonianTerm(linspace(-2.0, 2.0, 5));
        HamiltonianBlock H2(5, 1, {&diagonal});
        H2.diagonalise();
        CHECK(H2.averageEnergy(1) == Approx(-1.451941567662));
        CHECK(H2.averageEnergy(0.5) == Approx(-0.9056333666324617));
    }
}
