#include "test_utils.h"

using namespace std;
using namespace arma;

void testVectorApprox(vector<double> v1, vector<double> v2) {
    CHECK(v1.size() == v2.size());
    for (size_t i = 0; i < v1.size(); ++i) {
        REQUIRE(v1[i] == Approx(v2[i]));
    }
}

void testVectorApprox(vec v1, vec v2) {
    CHECK(approx_equal(v1, v2, "absdiff", 1e-5));
}

void testVectorApprox(cx_vec v1, cx_vec v2) {
    CHECK(approx_equal(v1, v2, "absdiff", 1e-5));
}

void testComplexApprox(std::complex<double> c1, std::complex<double> c2) {
    CHECK(c1.real() == Approx(c2.real()).margin(1e-10));
    CHECK(c1.imag() == Approx(c2.imag()).margin(1e-10));
}

arma::vec TestUtils::createRandomVector(uint n, bool normalised) {
    vec v = randu(n);
    if (normalised)
        v = normalise(v);
    return v;
}

arma::cx_vec TestUtils::createRandomCplxVector(uint n, bool normalised) {
    cx_vec v;
    v.randu(n);
    if (normalised)
        v = normalise(v);
    return v;
}