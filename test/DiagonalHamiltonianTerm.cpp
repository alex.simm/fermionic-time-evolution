#include "DiagonalHamiltonianTerm.h"
#include "../src/utils.h"

using namespace std;
using namespace arma;

DiagonalHamiltonianTerm::DiagonalHamiltonianTerm(vec eigenvalues) : HamiltonianTerm(true),
                                                                    eigenvalues(eigenvalues) {
}

DiagonalHamiltonianTerm::DiagonalHamiltonianTerm(const DiagonalHamiltonianTerm &s) : HamiltonianTerm(s) {
    this->operator=(s);
}

DiagonalHamiltonianTerm &DiagonalHamiltonianTerm::operator=(const DiagonalHamiltonianTerm &s) {
    if (&s != this) {
        eigenvalues = s.eigenvalues;
    }

    return *this;
}

void DiagonalHamiltonianTerm::addEntries(TermCallback *callback, const Basis &basis, set<uint> bandSites) {
    for (const State &state : basis.getStates()) {
        ulong stateIdx = basis.getStateIndex(state);
        callback->addEntry(eigenvalues[stateIdx], state, state, stateIdx, stateIdx);
    }
}

std::string DiagonalHamiltonianTerm::toString() const {
    stringstream s;
    s << "diagonal (" << Utils::convert<vec, double>(eigenvalues) << ")";
    return s.str();
}