#include "test_utils.h"
#include "../src/utils.h"
#include <iostream>

using namespace std;
using namespace arma;
using Catch::Matchers::Approx;

TEST_CASE("Utils functions tests") {

    SECTION("Test convert stl -> arma") {
        uint size = 10;
        vec rand = randu(size);

        std::vector<double> v1 = Utils::convert<vec, double>(rand);
        vec v2 = Utils::convert<double, vec>(v1);

        CHECK(v1.size() == v2.n_rows);
        for (uint i = 0; i < size; ++i) {
            CHECK(v1[i] == v2(i));
        }
    }

    SECTION("Test convert arma -> stl") {
        uint size = 10;
        vec v1 = randu(size);
        std::vector<double> v2 = Utils::convert<vec, double>(v1);

        CHECK(v1.n_rows == v2.size());
        for (uint i = 0; i < size; ++i) {
            CHECK(v1(i) == v2[i]);
        }
    }

    SECTION("Test applyTo vector function") {
        vector<double> v = {1, 2, 3, 4, 5};

        vector<double> v1 = Utils::applyTo<double>(v, [](const double x) { return 1.0 / x; });
        vector<double> result1 = {1.0, 1.0 / 2, 1.0 / 3, 1.0 / 4, 1.0 / 5};
        CHECK_THAT(v1, Approx(result1));

        vector<double> v2 = Utils::applyTo<double>(v, [](const double x) { return std::exp(x); });
        vector<double> result2 = {2.7182818285, 7.3890560989, 20.0855369232, 54.5981500331, 148.4131591026};
        CHECK_THAT(v2, Approx(result2));
    }

    SECTION("Add and subtract arma vectors") {
        vec v = Utils::convert<double, vec>({1, 2, 3, 4, 5, 6, 7, 8});
        vec v2 = v + 3;
        CHECK(approx_equal(v2, Utils::convert<double, vec>({4, 5, 6, 7, 8, 9, 10, 11}), "absdiff", 1e-10));

        v = linspace(-5.3, 3.8, 17);
        v2 = v - 1.2;
        for (int i = 0; i < v.size(); ++i) {
            CHECK(v2[i] == v[i] - 1.2);
        }
    }

    SECTION("Linear spaced vector") {
        CHECK(Utils::linSpaced(10, -3, 6) == vector<double>{-3, -2, -1, 0, 1, 2, 3, 4, 5, 6});
        CHECK(Utils::linSpaced(10, 1, 10) == vector<double>{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        CHECK(Utils::linSpaced(6, 3, 4) == vector<double>{3.0, 3.2, 3.4, 3.6, 3.8, 4.0});
        CHECK(Utils::linSpaced(11, 7, 6) == vector<double>{7.0, 6.9, 6.8, 6.7, 6.6, 6.5, 6.4, 6.3, 6.2, 6.1, 6.0});
    }

    SECTION("First in vector") {
        vector<double> v = Utils::linSpaced(11, 0.0, 1.0);
        CHECK(Utils::firstIndexGreaterThan(v, 0.45) == 5);
        CHECK(Utils::firstIndexGreaterThan(v, 0.5) == 6);
        CHECK(Utils::firstIndexGreaterThan(v, 0.5, true) == 5);
        CHECK(Utils::firstIndexGreaterThan(v, -3.0) == 0);
        CHECK_THROWS(Utils::firstIndexGreaterThan(v, 1.1, true));

        v = Utils::linSpaced(11, -1.0, 1.0);
        CHECK(Utils::firstIndexGreaterThan(v, -0.1) == 5);
        CHECK(Utils::firstIndexGreaterThan(v, 0.0) == 6);
        CHECK(Utils::firstIndexGreaterThan(v, 0.0, true) == 5);
        CHECK(Utils::firstIndexGreaterThan(v, -3.0) == 0);
        CHECK_THROWS(Utils::firstIndexGreaterThan(v, 1.1, true));
    }
}
