#include "../../src/hamiltonians/ChemicalPotential.h"
#include "../../src/HamiltonianBlock.h"
#include "../test_utils.h"
#include "../../src/utils.h"

using namespace std;
using namespace arma;

TEST_CASE("Chemical potential Hamiltonian term class tests") {

    SECTION("Hamiltonian is hermitian") {
        ChemicalPotential cp(1);
        HamiltonianBlock H(8, 4, {&cp});

        cx_mat m = H.asMatrix();
        for (int i = 0; i < m.n_rows; ++i) {
            CHECK(imag(m(i, i)) < 1e-10);

            for (int j = 0; j < i; ++j) {
                testComplexApprox(m(i, j), conj(m(j, i)));
            }
        }

        CHECK(approx_equal(H.asMatrix().t(), H.asMatrix(), "absdiff", 1e-10));
    }

    SECTION("Real eigenvalues") {
        ChemicalPotential cp(1);
        HamiltonianBlock H(8, 4, {&cp});

        vec evals;
        cx_mat evecs;
        bool success = eig_sym(evals, evecs, H.asMatrix());
        CHECK(success);

        CHECK(approx_equal(imag(evals), vec(evals.size(), fill::zeros), "absdiff", 1e-5));

        H.diagonalise();
        CHECK(approx_equal(H.getEigenvalues(), evals, "absdiff", 1e-5));
    }
}
