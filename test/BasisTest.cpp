#include <iostream>
#include "test_utils.h"
#include "../src/utils.h"
#include "../src/Basis.h"

using namespace std;
using namespace arma;

TEST_CASE("Basis class tests") {

    auto extractStateNumbersLambda = [](const State s) { return s.getStateNumber(); };

    SECTION("Single particle states") {
        Basis b(5, 1);

        CHECK(b.getN() == 5);
        CHECK(b.getQ() == 1);
        CHECK(b.dim() == 5);
        CHECK(Utils::applyTo<ulong>(b.getStates(), extractStateNumbersLambda) == std::vector<ulong>({1, 2, 4, 8, 16}));
    }

    SECTION("Many particle states") {
        Basis b1(4, 2);
        CHECK(b1.getN() == 4);
        CHECK(b1.getQ() == 2);
        CHECK(Utils::applyTo<ulong>(b1.getStates(), extractStateNumbersLambda) ==
              std::vector<ulong>({3, 5, 6, 9, 10, 12}));

        Basis b2(10, 7);
        CHECK(b2.getN() == 10);
        CHECK(b2.getQ() == 7);
        CHECK(Utils::applyTo<ulong>(b2.getStates(), extractStateNumbersLambda) ==
              std::vector<ulong>({127, 191, 223, 239, 247,
                                  251, 253, 254, 319, 351,
                                  367, 375, 379, 381, 382,
                                  415, 431, 439, 443, 445,
                                  446, 463, 471, 475, 477,
                                  478, 487, 491, 493, 494,
                                  499, 501, 502, 505, 506,
                                  508, 575, 607, 623, 631,
                                  635, 637, 638, 671, 687,
                                  695, 699, 701, 702, 719,
                                  727, 731, 733, 734, 743,
                                  747, 749, 750, 755, 757,
                                  758, 761, 762, 764, 799,
                                  815, 823, 827, 829, 830,
                                  847, 855, 859, 861, 862,
                                  871, 875, 877, 878, 883,
                                  885, 886, 889, 890, 892,
                                  911, 919, 923, 925, 926,
                                  935, 939, 941, 942, 947,
                                  949, 950, 953, 954, 956,
                                  967, 971, 973, 974, 979,
                                  981, 982, 985, 986, 988,
                                  995, 997, 998, 1001,
                                  1002,
                                  1004, 1009, 1010, 1012,
                                  1016}));
    }

    SECTION("Variable particle number basis") {
        for (uint N = 2; N < 20; ++N) {
            Basis b(N, -1);
            CHECK(b.getN() == N);
            CHECK(b.getQ() == -1);
            CHECK(b.dim() == (ulong) pow(2, N));
        }
    }

    SECTION("Get state") {
        Basis b(4, 4);

        CHECK(b.getState(0) == State(4, 15));
        CHECK_THROWS(b.getState(1));

        vec evals2({1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0});
        Basis b2(8, 4);
        const std::vector<State> &states = b2.getStates();
        for (int i = 0; i < states.size(); ++i) {
            CHECK(b2.getState(i) == states[i]);
        }
        for (int i = states.size(); i < states.size() + 10; ++i) {
            CHECK_THROWS(b.getState(i));
        }
    }

    SECTION("State index") {
        Basis b(4, 4);
        CHECK(b.getStateIndex(State(4, 15)) == 0);
        CHECK(b.getStateIndex(State(4, 13)) == -1);

        Basis b2(8, 4);
        const std::vector<State> &states = b2.getStates();
        for (int i = 0; i < states.size(); ++i) {
            CHECK(b2.getStateIndex(states[i]) == i);
        }

        // a zero-state should not have a valid index, even if it is in the state space
        State state = b2.getState(0);
        state.create(5);
        state.annihilate(0);
        state.annihilate(0);
        CHECK(state.isZero());
        CHECK(b2.getStateIndex(state) == -1);
    }
}

