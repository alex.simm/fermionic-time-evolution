#include "EmptySimulation.h"

EmptySimulation::EmptySimulation(Hamiltonian *hamiltonian) : Simulation(hamiltonian) {
}

EmptySimulation::EmptySimulation(const EmptySimulation &s) : Simulation(s) {
    this->operator=(s);
}

class EmptySimulation &EmptySimulation::operator=(const EmptySimulation &s) {
    if (this != &s) {
        Simulation::operator=(s);
    }

    return *this;
}

void EmptySimulation::evolve(double t) {
}