#include "../test_utils.h"
#include "../../src/utils.h"
#include "../../src/simulations/ExactSimulation.h"
#include "../../src/HamiltonianBlock.h"
#include "../DiagonalHamiltonianTerm.h"

using namespace std;
using namespace arma;

TEST_CASE("Exact simulation class tests") {

    SECTION("Auto diagonalisation") {
        vec evals = randu(10);
        DiagonalHamiltonianTerm diagonal(evals);
        HamiltonianBlock H(evals.size(), 1, {&diagonal});

        ExactSimulation sim(&H, true);
        CHECK_FALSE(H.isDiagonalised());
        sim.evolve(0.0);
        CHECK(H.isDiagonalised());

        std::vector<double> sorted = Utils::convert<vec, double>(evals);
        std::sort(sorted.begin(), sorted.end());
        CHECK(approx_equal(H.getEigenvalues(), Utils::convert<double, vec>(sorted), "absdiff", 1e-5));
    }

    SECTION("Exact evolution in diagonal Hamiltonian") {
        vec evals = {1, 1, 1};
        DiagonalHamiltonianTerm diagonal(evals);
        HamiltonianBlock H(evals.size(), 1, {&diagonal});

        ExactSimulation sim(&H, true);
        sim.setPsi(H.getBasis().getState(0));

        sim.evolve(0);
        CHECK(approx_equal(sim.getPsi(), cx_vec({1.0, 0.0, 0.0}), "absdiff", 1e-5));
        sim.evolve(M_PI / 2.0);
        CHECK(approx_equal(sim.getPsi(), cx_vec({-1.0i, 0.0, 0.0}), "absdiff", 1e-5));
        sim.evolve(-M_PI - M_PI / 4);
        CHECK(approx_equal(sim.getPsi(), cx_vec({(1.0i - 1.0) * M_SQRT1_2, 0.0, 0.0}), "absdiff", 1e-5));
    }

    //TODO: test for block diagonal Hamiltonian
}
