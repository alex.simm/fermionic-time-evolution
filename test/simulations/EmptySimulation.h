#ifndef IMPURITY_MODEL_EMPTYSIMULATION_H
#define IMPURITY_MODEL_EMPTYSIMULATION_H

#include "../../src/simulations/Simulation.h"

/**
 * An empty simulation implementation for testing purposes that does nothing.
 */
class EmptySimulation : public Simulation {
public:
    explicit EmptySimulation(Hamiltonian *hamiltonian);

    EmptySimulation(const EmptySimulation &s);

    ~EmptySimulation() = default;

    EmptySimulation &operator=(const EmptySimulation &s);

    void evolve(double t) override;
};

#endif //IMPURITY_MODEL_EMPTYSIMULATION_H
