#include "../src/State.h"
#include "test_utils.h"

using namespace std;
using namespace arma;

TEST_CASE("State class tests") {
    SECTION("Construction") {
        State state1(6, 0);
        CHECK(state1.getOccupiedSites().empty());
        CHECK(state1.getStateNumber() == 0);

        State state2(6, 7);
        CHECK(state2.getOccupiedSites() == vector<uint>{0, 1, 2});

        State state3(6, 25);
        CHECK(state3.getOccupiedSites() == vector<uint>{0, 3, 4});
    }

    SECTION("State numbers") {
        for (ulong j = 0; j < (1 << 8ul); ++j) {
            State state(8, j);
            CHECK(state.getStateNumber() == j);
        }
    }

    SECTION("Occupied sites") {
        for (uint j = 0; j < 20; ++j) {
            CHECK_FALSE(State(5, 0).isSiteOccupied(j));
        }

        CHECK_FALSE(State(5, 2).isSiteOccupied(0));
        CHECK(State(5, 2).isSiteOccupied(1));
        CHECK_FALSE(State(5, 2).isSiteOccupied(2));

        for (uint j = 0; j < 10; ++j) {
            CHECK(State(10, (1u << 10) - 1).isSiteOccupied(j));
        }
    }

    SECTION("Number of particles") {
        const uint N = 5;

        vec evals = linspace(1.0, 10.0, 10);

        CHECK(State(N, 0).numParticles() == 0);
        CHECK(State(N, 1).numParticles() == 1);
        CHECK(State(N, 15).numParticles() == 4);
        CHECK(State(N, 10).numParticles() == 2);
    }

    SECTION("Number of different sites") {
        const uint N = 5;

        CHECK(State(N, 1).numDifferentSites(State(N, 0)) == 1);
        CHECK(State(N, 15).numDifferentSites(State(N, 0)) == 4);
        CHECK(State(N, 16).numDifferentSites(State(N, 0)) == 1);
        CHECK(State(N, 31).numDifferentSites(State(N, 0)) == 5);
        CHECK(State(N, 31).numDifferentSites(State(N, 15)) == 1);
        CHECK(State(N, 4).numDifferentSites(State(N, 2)) == 2);
        CHECK(State(N, 14).numDifferentSites(State(N, 3)) == 3);
    }

    SECTION("Number of occupied sites") {
        const uint N = 6;

        CHECK(State(N, 1).numOccupiedSites(0, 5) == 1);
        CHECK(State(N, 1).numOccupiedSites(1, 5) == 0);
        CHECK(State(N, 15).numOccupiedSites(1, 3) == 3);
        CHECK(State(N, 16).numOccupiedSites(2, 7) == 1);
        CHECK(State(N, 31).numOccupiedSites(2, 7) == 3);
        CHECK(State(N, 31).numOccupiedSites(1, 6) == 4);
        CHECK(State(N, 4).numOccupiedSites(1, 4) == 1);
        CHECK(State(N, 14).numOccupiedSites(0, 3) == 3);
    }

    SECTION("Creation operator") {
        const uint N = 6;

        State state1(N, 0);
        state1.create(5);
        CHECK(state1.getCoefficient() == 1);
        CHECK(state1.numParticles() == 1);
        CHECK(state1.getOccupiedSites() == vector<uint>{5});

        State state2(N, 2);
        state2.create(5);
        CHECK(state2.getCoefficient() == -1);
        CHECK(state2.numParticles() == 2);
        CHECK(state2.getOccupiedSites() == vector<uint>{1, 5});

        State state3(N, 4);
        state3.create(2);
        CHECK(state3.getCoefficient() == 0);
    }

    SECTION("Annihilation operator") {
        const uint N = 6;

        State state1(N, 25);
        state1.annihilate(0);
        CHECK(state1.getCoefficient() == 1);
        CHECK(state1.numParticles() == 2);
        CHECK(state1.getOccupiedSites() == vector<uint>{3, 4});

        State state2(N, 25);
        state2.annihilate(3);
        CHECK(state2.getCoefficient() == -1);
        CHECK(state2.numParticles() == 2);
        CHECK(state2.getOccupiedSites() == vector<uint>{0, 4});

        State state3(N, 4);
        state3.annihilate(3);
        CHECK(state3.getCoefficient() == 0);
    }

    SECTION("Applying quadratic term") {
        State state(6, 15);

        State state2 = state.applyTerm(4, 3);
        CHECK(state2.getStateNumber() == 23);

        State state3 = state.applyTerm(4, 0);
        CHECK(state3.getStateNumber() == 30);

        State state4 = state.applyTerm(3, 4);
        CHECK(state4.isZero());
    }

    SECTION("Applying quartic term") {
        State state(5, 15);

        State state2 = state.applyTerm(4, 3, 3, 2);
        CHECK(state2.getStateNumber() == 27);

        State state3 = state.applyTerm(6, 7, 1, 2);
        CHECK(state3.getStateNumber() == 201);

        State state4 = state.applyTerm(2, 2, 1, 1);
        CHECK(state4.isZero());

        State state5 = state.applyTerm(7, 6, 5, 4);
        CHECK(state5.isZero());
    }

    SECTION("Combining states") {
        State stateA(4, 0b1010);
        State stateB(2, 0b11);
        vector<uint> sitesA = {0, 1, 2, 3};
        vector<uint> sitesB = {4, 5};
        State combined = State::combineStates(stateA, sitesA, stateB, sitesB);
        CHECK(combined.getStateNumber() == 0b111010);
        CHECK(combined.numParticles() == 4);
        CHECK(combined.getOccupiedSites() == vector<uint>{1, 3, 4, 5});
        CHECK(combined.getCoefficient() == 1);

        stateA = State(4, 0b1111);
        stateB = State(4, 0b0000);
        sitesA = vector<uint>{0, 2, 4, 6};
        sitesB = vector<uint>{1, 3, 5, 7};
        combined = State::combineStates(stateA, sitesA, stateB, sitesB);
        CHECK(combined.getStateNumber() == 0b01010101);
        CHECK(combined.numParticles() == 4);
        CHECK(combined.getOccupiedSites() == sitesA);
        CHECK(combined.getCoefficient() == 1);
    }
}
