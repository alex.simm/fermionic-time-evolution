#include "BlockBasisStateSelector.h"
#include "../BlockDiagonalHamiltonian.h"
#include "../Log.h"
#include "../utils.h"

using namespace std;

BlockBasisStateSelector::BlockBasisStateSelector(uint nth) : nth(nth) {
}

void BlockBasisStateSelector::initialise(Simulation *sim) {
    Hamiltonian *H = sim->getHamiltonian();
    if (HamiltonianBlock *block = dynamic_cast<HamiltonianBlock *>(H)) {
        State state = block->getBasis().getState(nth);
        Log::debug() << "initialising in basis state: " << state << endl;
        sim->setPsi(state);
    } else if (BlockDiagonalHamiltonian *bd = dynamic_cast<BlockDiagonalHamiltonian *>(H)) {
        vector<State> states = Utils::applyTo<State>(bd->getBlocks(), [this](const HamiltonianBlock *block) {
            return block->getBasis().getState(nth);
        });
        Log::debug() << "initialising in basis states: " << states << endl;
        sim->setPsi(states);
    } else {
        throw runtime_error("unknown Hamiltonian type");
    }
}

string BlockBasisStateSelector::toString() {
    stringstream stream;
    stream << "basis state per block (" << nth << ")";
    return stream.str();
}