#ifndef IMPURITY_MODEL_CLIPARSER_H
#define IMPURITY_MODEL_CLIPARSER_H

#include <string>
#include "HamiltonianCreator.h"
#include "../hamiltonians/HamiltonianTerm.h"

/**
 * Utility class that parses command line options and returns corresponding objects.
 */
class CLIParser {
public:

    /**
     * Parses one term for a Hamiltonian.
     *
     * @param param a comma-separated list of the parameters
     */
    static HamiltonianCreator *parseHamiltonian(std::string param);

    /**
     * Parses one term for a Hamiltonian.
     *
     * @param param a comma-separated list of the type and parameters
     * @param N number of sites in the Hamiltonian
     */
    static HamiltonianTerm *parseHamiltonianTerm(std::string param, uint N);
};


#endif //IMPURITY_MODEL_CLIPARSER_H
