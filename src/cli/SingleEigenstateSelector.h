#ifndef IMPURITY_MODEL_SINGLEEIGENSTATESELECTOR_H
#define IMPURITY_MODEL_SINGLEEIGENSTATESELECTOR_H

#include "StateSelector.h"

/**
 * Initialises a simulation in an eigenstate of the Hamiltonian.
 */
class SingleEigenstateSelector : public StateSelector {
public:
    int nth;
    int Q;
    double energy;

    /**
     * Creates an instance that selects the n-th eigenstate of the Hamiltonian, where 0 is the ground state. If Q
     * is a valid fermion number (>=0, <=N), the index will be taken with respect to the specific block. Else the
     * state will be selected from all eigenstates of the Hamiltonian.
     *
     * @param nth
     * @param Q fermion number of the block to choose from, or -1 to choose from all eigenstates
     */
    SingleEigenstateSelector(uint nth, int Q = -1);

    /**
     * Creates an instance that selects the first eigenstate of the Hamiltonian whose energy is greater or equal
     * to the given value. If Q is a valid fermion number (>=0, <=N), only the eigenstates from that block will
     * be considered. Else the state will be selected from all eigenstates of the Hamiltonian.
     *
     * @param energy minimal energy of the eigenstate
     * @param Q fermion number of the block to choose from, or -1 to choose from all eigenstates
     * @throws runtime_error if the energy is NaN
     */
    SingleEigenstateSelector(double energy, int Q = -1);

    ~SingleEigenstateSelector() = default;

    void initialise(Simulation *sim) override;

    std::string toString() override;
};

#endif //IMPURITY_MODEL_SINGLEEIGENSTATESELECTOR_H
