#ifndef IMPURITY_MODEL_SIMULATIONSELECTOR_H
#define IMPURITY_MODEL_SIMULATIONSELECTOR_H

#include <string>
#include <vector>
#include "../Hamiltonian.h"
#include "../simulations/Simulation.h"
#include "../utils.h"

/**
 * Abstract base class for any class that can repeatedly create simulations. Also stores parameters for time
 * evolution.
 */
class SimulationSelector {
public:
    double dt;
    uint nt;

    virtual ~SimulationSelector() = default;

    /**
     * Creates a new instance. The time step and number
     *
     * @param dt
     * @param nt
     */
    SimulationSelector(double dt, uint nt) : dt(dt), nt(nt) {}

    virtual Simulation *createSimulation(Hamiltonian *H) = 0;

    virtual std::string toString() = 0;

    /**
     * Creates a list of time stamps for the specified time steps.
     */
    std::vector<double> createTimeVector() {
        return Utils::linSpaced(nt, 0.0, nt * dt);
    }
};

#endif //IMPURITY_MODEL_SIMULATIONSELECTOR_H
