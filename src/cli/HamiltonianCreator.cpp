#include "HamiltonianCreator.h"
#include "../Log.h"
#include "../HamiltonianBlock.h"
#include "../BlockDiagonalHamiltonian.h"

using namespace std;

HamiltonianCreator::HamiltonianCreator(uint N, uint Q, bool useImpurity, double Eimp, double V, bool useSparse)
        : N(N), Q(Q), terms(0), useImpurity(useImpurity), Eimp(Eimp), V(V), useSparse(useSparse) {}

HamiltonianCreator::~HamiltonianCreator() {
    for (uint i = 0; i < terms.size(); i++) {
        delete terms[i];
        terms[i] = nullptr;
    }
}

Hamiltonian *HamiltonianCreator::createHamiltonian() {
    Log::debug() << "creating: " << toString() << endl;

    Hamiltonian *H;
    if (Q < 0) {
        if (useImpurity)
            H = new BlockDiagonalHamiltonian(N, terms, Eimp, V, useSparse);
        else
            H = new BlockDiagonalHamiltonian(N, terms, useSparse);
    } else {
        if (useImpurity)
            H = new HamiltonianBlock(N, Q, terms, Eimp, V, useSparse);
        else
            H = new HamiltonianBlock(N, Q, terms, useSparse);
    }

    return H;
}

string HamiltonianCreator::toString() {
    stringstream stream;
    stream << "Hamiltonian: (N=" << N << ", Q=" << Q << ")";
    if (useImpurity)
        stream << ", impurity: (Eimp=" << Eimp << ", V=" << V << ")";
    stream << ", " << (useSparse ? "sparse" : "dense");
    stream << ", terms: ";
    for (HamiltonianTerm *term:terms) {
        stream << term->toString() << ", ";
    }
    return stream.str();
}

uint HamiltonianCreator::numSites() {
    return N + (useImpurity ? 1 : 0);
}