#include "SingleEigenstateSelector.h"
#include "../BlockDiagonalHamiltonian.h"
#include "../utils.h"
#include "../Log.h"
#include <stdexcept>

using namespace std;
using namespace arma;

SingleEigenstateSelector::SingleEigenstateSelector(uint nth, int Q) : nth(nth), Q(Q), energy(NAN) {
}

SingleEigenstateSelector::SingleEigenstateSelector(double energy, int Q) : nth(-1), Q(Q), energy(energy) {
    if(isnan(energy))
        throw new runtime_error("invalid energy value");
}


void SingleEigenstateSelector::initialise(Simulation *sim) {
    Hamiltonian *H = sim->getHamiltonian();

    // select Hilbert or Fock space
    if (Q >= 0) {
        if (HamiltonianBlock * block = dynamic_cast<HamiltonianBlock *>(H)) {
            if (block->getBasis().getQ() != Q)
                throw runtime_error("basis does not contain states with fermion number " + to_string(Q));
        } else if (BlockDiagonalHamiltonian * bd = dynamic_cast<BlockDiagonalHamiltonian *>(H)) {
            H = bd->getBlock(Q);
        }
    }

    // find the index based on the energy, if any given
    if (!isnan(energy))
        nth = Utils::firstIndexGreaterThan(Utils::convert<vec, double>(H->getEigenvalues()), energy);

    if (!H->isDiagonalised() || H->getEigenvalues().size() < nth) {
        Log::debug() << "diagonalising (with eigenvectors, "
                     << ((nth == 0) ? "ground state only" : "all eigenvalues") << ")" << endl;
        H->diagonalise(true, nth == 0);
    }

    Log::debug() << "initialising in eigenvector " << nth << endl;
    sim->setPsi(H->getEigenvectors().col(nth));
}

string SingleEigenstateSelector::toString() {
    return "eigenstate " + to_string(nth);
}