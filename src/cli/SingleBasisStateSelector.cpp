#include "SingleBasisStateSelector.h"
#include "../Log.h"
#include "../HamiltonianBlock.h"
#include "../BlockDiagonalHamiltonian.h"
#include "../simulations/SimulationUtils.h"
#include <stdexcept>

using namespace std;

void SingleBasisStateSelector::initialise(Simulation *sim) {
    Hamiltonian *H = sim->getHamiltonian();

    // select Hilbert or Fock space
    if (Q >= 0) {
        if (HamiltonianBlock *block = dynamic_cast<HamiltonianBlock *>(H)) {
            if (block->getBasis().getQ() != Q)
                throw runtime_error("basis does not contain states with fermion number " + to_string(Q));
        } else if (BlockDiagonalHamiltonian *bd = dynamic_cast<BlockDiagonalHamiltonian *>(H)) {
            H = bd->getBlock(Q);
        } else {
            throw runtime_error("unknown Hamiltonian type");
        }
    }
    vector<State> states = H->getBasis().getStates();

    // filter by occupied site
    if(occupiedSite>=0 && occupiedSite < H->getBasis().getN()) {
        states = SimulationUtils::getStatesWithOccupiedSite(states, occupiedSite, true);
    }

    // select state
    State state = H->getBasis().getState(nth);
    Log::debug() << "initialising in basis state: " << state << endl;
    sim->setPsi(state);
}

std::string SingleBasisStateSelector::toString() {
    return "basis state " + to_string(nth);
}