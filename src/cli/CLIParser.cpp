#include "CLIParser.h"
#include "../utils.h"
#include "../hamiltonians/OnSiteEnergy.h"
#include "../hamiltonians/InteractionTerm.h"
#include "../hamiltonians/ChemicalPotential.h"
#include "../hamiltonians/SYK2.h"
#include "../hamiltonians/SYK4.h"
#include "../hamiltonians/Hopping.h"

using namespace std;

HamiltonianCreator *CLIParser::parseHamiltonian(string param) {
    vector<string> params = Utils::split(param, '/');
    if (params.size() < 1)
        throw runtime_error("Hamiltonian param not parsable");

    // main parameters for Hamiltonian
    vector<string> mainParams = Utils::split(params[0], ',');
    if (mainParams.size() < 5)
        throw runtime_error("main parameters for Hamiltonian: N,Q,useImpurity,Eimp,V,[sparse]");
    params.erase(params.begin());

    // parse main parameters
    uint N = atoi(mainParams[0].c_str());
    uint Q = atoi(mainParams[1].c_str());
    bool useImpurity = atoi(mainParams[2].c_str()) != 0;
    double Eimp = atof(mainParams[3].c_str());
    double V = atof(mainParams[4].c_str());
    bool useSparse = (mainParams.size() > 5) && (mainParams[5] == "sparse");
    HamiltonianCreator *creator = new HamiltonianCreator(N, Q, useImpurity, Eimp, V, useSparse);

    // parse Hamiltonian terms
    for (string s : params) {
        creator->terms.push_back(parseHamiltonianTerm(s, N));
    }
    return creator;
}

HamiltonianTerm *CLIParser::parseHamiltonianTerm(string param, uint N) {
    vector<string> params = Utils::split(param, ',');
    if (params.size() < 1)
        throw runtime_error("Hamiltonian param not parsable");

    string type = params[0];
    params.erase(params.begin());

    if (type == "onsite") {
        if (params.size() < 1)
            throw runtime_error("parameters for on-site energy: bandwidth");
        double bandWidth = atof(params[0].c_str());
        return new OnSiteEnergy(Utils::linSpaced(N, -0.5 * bandWidth, 0.5 * bandWidth));
    } else if (type == "chem") {
        if (params.size() < 1)
            throw runtime_error("parameters for on-site energy: mu");
        double mu = atof(params[0].c_str());
        return new ChemicalPotential(mu);
    } else if (type == "hopping") {
        if (params.size() < 1)
            throw runtime_error("parameters for hopping: energy");
        double t = atof(params[0].c_str());
        return new Hopping(t);
    } else if (type == "int") {
        if (params.size() < 1)
            throw runtime_error("parameters for interaction: strength");
        double strength = atof(params[0].c_str());
        return new InteractionTerm(strength);
    } else if (type == "syk2") {
        if (params.size() < 1)
            throw runtime_error("parameters for hopping: J");
        double J = atof(params[0].c_str());
        return new SYK2(N, J);
    } else if (type == "syk4") {
        if (params.size() < 1)
            throw runtime_error("parameters for hopping: J");
        double J = atof(params[0].c_str());
        return new SYK4(N, J);
    } else if (type == "int") {
        if (params.size() < 1)
            throw runtime_error("parameters for interaction: strength");
        double strength = atof(params[0].c_str());
        return new InteractionTerm(strength);
    } else {
        throw runtime_error(
                "valid Hamiltonian terms: 'onsite' (on-site energy), 'chem' (chemical potential), 'hopping' (constant hopping), 'syk2' (SYK q=2), 'syk4' (SYK q=4), 'int' (constant interaction)");
    }
}
