#ifndef IMPURITY_MODEL_LANCZOSSIMULATIONSELECTOR_H
#define IMPURITY_MODEL_LANCZOSSIMULATIONSELECTOR_H

#include "SimulationSelector.h"
#include "../simulations/LanczosSimulation.h"

class LanczosSimulationSelector : public SimulationSelector {
public:
    uint numLanczosVectors;

    LanczosSimulationSelector(double dt, uint nt, uint numLanczosVectors);

    Simulation *createSimulation(Hamiltonian *H) override;

    std::string toString() override;
};

#endif //IMPURITY_MODEL_LANCZOSSIMULATIONSELECTOR_H
