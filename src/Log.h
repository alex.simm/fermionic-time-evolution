#ifndef IMPURITY_MODEL_LOG_H
#define IMPURITY_MODEL_LOG_H

#include <string>
#include <iostream>

class NullBuffer : public std::streambuf {
public:
    int overflow(int c) { return c; }
};

class Log {
public:
    enum Level {
        NONE = 0, INFO = 1, DEBUG = 2
    };

    Log(const Log &l) = delete;

    void operator=(const Log &l) = delete;

    static Log &getInstance();

    Level getLevel() const;

    void setLevel(Level l);

    static std::ostream &info();

    static std::ostream &debug();

    std::ostream &log(Level level);

private:
    Log();

    Level logLevel;
    NullBuffer nullBuffer;
    std::ostream nullStream;
};

#endif //IMPURITY_MODEL_LOG_H
