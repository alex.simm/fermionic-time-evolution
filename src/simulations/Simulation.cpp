#include "Simulation.h"
#include "../HamiltonianBlock.h"
#include <stdexcept>

using namespace std;
using namespace arma;

Simulation::Simulation(Hamiltonian *system) : system(system), psi(system->getBasis().dim(), fill::zeros),
                                              times(0), time(0) {
}

Simulation::Simulation(const Simulation &s) {
    this->operator=(s);
}

Simulation &Simulation::operator=(const Simulation &s) {
    if (this != &s) {
        system = s.system;
        psi = s.psi;
        times = s.times;
        time = s.time;
    }

    return *this;
}

Hamiltonian *Simulation::getHamiltonian() {
    return system;
}

cx_vec Simulation::getPsi() const {
    return psi;
}

void Simulation::setPsi(const cx_vec &newPsi) {
    if (newPsi.size() != system->getBasis().dim())
        throw runtime_error("wrong size of new state vector psi: " + to_string(newPsi.size())
                            + " != " + to_string(system->getBasis().dim()));
    psi = newPsi;
}

void Simulation::setPsi(State state) {
    psi.fill(0.0);
    psi[system->getBasis().getStateIndex(state)] = 1;
}

void Simulation::setPsi(vector<State> states) {
    setPsi(states, vector<complex<double>>(states.size(), 1.0));
}

void Simulation::setPsi(vector<State> states, vector<complex<double>> weights) {
    if (states.size() != weights.size())
        throw runtime_error("List of states and list of weights have different sizes");

    psi.fill(0.0);
    for (uint i = 0; i < states.size(); i++) {
        psi[system->getBasis().getStateIndex(states[i])] = weights[i];
    }
    psi = normalise(psi);
}

double Simulation::getTime() const {
    return time;
}

const std::vector<double> &Simulation::getTimes() const {
    return times;
}

void Simulation::update(double t) {
    times.push_back(time);
    time += t;
}