#ifndef IMPURITY_MODEL_SIMULATIONUTILS_H
#define IMPURITY_MODEL_SIMULATIONUTILS_H

#include <vector>
#include "Simulation.h"
#include "../BlockDiagonalHamiltonian.h"
#include "ExactSimulation.h"
#include "../Observable.h"

class SimulationUtils {
public:

    /**
     * Returns all states from the given list in which the specified site is or is not occupied.
     * @param states a list of states
     * @param site a site that should (not) be occupied
     * @return a list of states, may be empty
     */
    static std::vector<State> getStatesWithOccupiedSite(const std::vector<State> &states, uint site,
                                                        bool occupied = true);

    /**
     * Returns a list with all states from the Hamiltonian in which the specified site is (or is not) occupied for each
     * block.
     * @param H a block-diagonal Hamiltonian
     * @return list of list of states
     */
    static std::vector<std::vector<State>> getStatesWithOccupiedSite(const BlockDiagonalHamiltonian *H, uint site,
                                                                     bool occupied = true);

    /**
     * Evolves the simulation by a number of time steps and calls the callback in every step. The simulations needs to
     * be initialised with a proper state.
     *
     * @param sim the simulation
     * @param nt number of time steps
     * @param dt time step
     * @param times vector that will be filled with the measurement times
     * @param measureSite index of the site that should be measured
     * @return the occupation of the site at each of the measurement times
     */
    static void evolve(Simulation *sim, uint nt, double dt, std::function<void(uint, uint)> callback,
                       std::vector<Observable *> observables = {});
};

#endif //IMPURITY_MODEL_SIMULATIONUTILS_H
