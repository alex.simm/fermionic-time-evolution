#ifndef IMPURITY_MODEL_SIMULATION_H
#define IMPURITY_MODEL_SIMULATION_H

#include <armadillo>
#include "../Hamiltonian.h"

class Simulation {
public:
    explicit Simulation(Hamiltonian *system);

    Simulation(const Simulation &s);

    virtual ~Simulation() = default;

    Simulation &operator=(const Simulation &s);

    virtual void evolve(double t) = 0;

    Hamiltonian *getHamiltonian();

    /** Returns the current state vector. */
    [[nodiscard]] arma::cx_vec getPsi() const;

    /**
     * Sets the current state vector.
     * @throws runtime_error if the dimension of the state does not match the Hilbert or Fock space
     */
    void setPsi(const arma::cx_vec &newPsi);

    /**
     * Sets the current state vector to the value corresponding to a basis state. The basis state must be in
     * the Hilbert or Fock space of the system.
     */
    void setPsi(State state);

    /**
     * Sets the current state vector to the values corresponding to a superposition of basis states. All
     * basis states will have the same weight and must be in the Hilbert or Fock space of the system.
     */
    void setPsi(std::vector<State> states);

    /**
     * Sets the current state vector to the values corresponding to a superposition of basis states with
     * different weights. All basis states will have the same weight and must be in the Hilbert or Fock space
     * of the system. The weights will be normalised.
     */
    void setPsi(std::vector<State> states, std::vector<std::complex<double>> weights);

    /** Returns the current time of the simulation */
    double getTime() const;

    /** Returns all current time points of the simulation */
    const std::vector<double> &getTimes() const;

protected:
    /** The simulated system */
    Hamiltonian *system;
    /** Current state vector */
    arma::cx_vec psi;

    /** Current time */
    double time;
    /** Time points during evolution */
    std::vector<double> times;

    /**
     * Updates the time vector and measures observables.
     */
    void update(double t);
};

#endif //IMPURITY_MODEL_SIMULATION_H
