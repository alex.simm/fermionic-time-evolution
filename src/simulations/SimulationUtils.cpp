#include "SimulationUtils.h"
#include "../Log.h"

using namespace std;

vector<State> SimulationUtils::getStatesWithOccupiedSite(const vector<State> &states, uint site, bool occupied) {
    vector<State> filtered;
    std::copy_if(states.begin(), states.end(), std::back_inserter(filtered),
                 [site, occupied](const State &s) { return s.isSiteOccupied(site) == occupied; });
    return filtered;
}

vector<vector<State>> SimulationUtils::getStatesWithOccupiedSite(const BlockDiagonalHamiltonian *H, uint site,
                                                                 bool occupied) {
    vector<vector<State>> v;

    for (HamiltonianBlock *h : H->getBlocks()) {
        v.push_back(getStatesWithOccupiedSite(h->getBasis().getStates(), site, occupied));
    }

    return v;
}

void SimulationUtils::evolve(Simulation *sim, uint nt, double dt, function<void(uint, uint)> callback,
                             vector<Observable *> observables) {
    for (uint i = 0; i < nt; ++i) {
        sim->evolve(dt);
        callback(i, nt);

        for (Observable *o : observables) {
            if (o != nullptr)
                o->measure(sim->getPsi(), i, nt);
        }
    }
}
