#ifndef IMPURITY_MODEL_LANCZOSSIMULATION_H
#define IMPURITY_MODEL_LANCZOSSIMULATION_H

#include "Simulation.h"

class LanczosSimulation : public Simulation {
public:
    LanczosSimulation(Hamiltonian *system, uint dimLanczos);

    LanczosSimulation(const LanczosSimulation &s);

    ~LanczosSimulation() = default;

    LanczosSimulation &operator=(const LanczosSimulation &s);

    void evolve(double t) override;

private:
    uint dimLanczos;

    void createLanczosBasis(arma::cx_mat &Q, arma::cx_mat &h) const;
};

#endif //IMPURITY_MODEL_LANCZOSSIMULATION_H
