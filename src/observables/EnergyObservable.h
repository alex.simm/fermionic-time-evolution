#ifndef IMPURITY_MODEL_ENERGYOBSERVABLE_H
#define IMPURITY_MODEL_ENERGYOBSERVABLE_H

#include "../Observable.h"
#include "../Hamiltonian.h"

class EnergyObservable : public Observable {
public:
    EnergyObservable(const Hamiltonian *H);

    double measure(const arma::cx_vec &psi);

    void measure(const arma::cx_vec &psi, uint step, uint numSteps) override;

    const std::vector<double> &getEnergies() const;

private:
    const Hamiltonian *H;
    std::vector<double> energies;
};

#endif //IMPURITY_MODEL_ENERGYOBSERVABLE_H
