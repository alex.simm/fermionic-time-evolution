#include "EntropyObservable.h"

using namespace arma;

EntropyObservable::EntropyObservable(const Basis &fullBasis, std::set<uint> sitesA)
        : calculator(fullBasis, sitesA) {
}

const std::vector<double> &EntropyObservable::getEntropy() const {
    return entropy;
}

void EntropyObservable::measure(const arma::cx_vec &psi, uint step, uint numSteps) {
    cx_mat rho = calculator.partialTrace(psi);
    entropy.push_back(EntropyCalculator::entropy(rho, true));
}