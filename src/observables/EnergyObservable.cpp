#include "EnergyObservable.h"

using namespace arma;

EnergyObservable::EnergyObservable(const Hamiltonian *H) : H(H) {
}

double EnergyObservable::measure(const arma::cx_vec &psi) {
    double E;

    if (H->isDiagonalised()) {
        const vec &evals = H->getEigenvalues();
        const cx_mat &evecs = H->getEigenvectors();

        E = 0;
        for (uint i = 0; i < H->getBasis().dim(); i++) {
            E += evals[i] * norm(cdot(psi, evecs.col(i)));
        }
    } else {
        if (H->isUsingSparseMatrix())
            E = cdot(psi, H->asSparseMatrix() * psi).real();
        else
            E = cdot(psi, H->asMatrix() * psi).real();
    }

    return E;
}

void EnergyObservable::measure(const arma::cx_vec &psi, uint step, uint numSteps) {
    energies.push_back(measure(psi));
}

const std::vector<double> &EnergyObservable::getEnergies() const {
    return energies;
}