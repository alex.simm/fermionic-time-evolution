#include "OccupationObservable.h"

using namespace std;

OccupationObservable::OccupationObservable(const Basis &basis)
        : basis(basis), occupations(basis.getN()) {
}

double OccupationObservable::calculateOccupation(const arma::cx_vec &psi, uint site) const {
    const vector<State> &states = basis.getStates();
    double d = 0;
    for (uint i = 0; i < psi.size(); ++i) {
        if (states[i].isSiteOccupied(site)) {
            d += norm(psi[i]);
        }
    }

    return d;
}

vector<double> OccupationObservable::calculateOccupations(const arma::cx_vec &psi) const {
    vector<double> v(basis.getN());
    for (uint i = 0; i < basis.getN(); i++) {
        v[i] = calculateOccupation(psi, i);
    }

    return v;
}

void OccupationObservable::measure(const arma::cx_vec &psi, uint step, uint numSteps) {
    for (uint i = 0; i < basis.getN(); i++) {
        occupations[i].push_back(calculateOccupation(psi, i));
    }
}

const std::vector<std::vector<double>> &OccupationObservable::getOccupations() const {
    return occupations;
}

const std::vector<double> &OccupationObservable::getOccupations(uint site) const {
    return occupations[site];
}
