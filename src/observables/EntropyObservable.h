#ifndef IMPURITY_MODEL_ENTROPYOBSERVABLE_H
#define IMPURITY_MODEL_ENTROPYOBSERVABLE_H

#include "../Observable.h"
#include "../utils/EntropyCalculator.h"
#include <armadillo>
#include <vector>

class EntropyObservable : public Observable {
public:
    EntropyObservable(const Basis &fullBasis, std::set<uint> sitesA);

    void measure(const arma::cx_vec &psi, uint step, uint numSteps) override;

    const std::vector<double> &getEntropy() const;

private:
    EntropyCalculator calculator;
    std::vector<double> entropy;
};

#endif //IMPURITY_MODEL_ENTROPYOBSERVABLE_H
