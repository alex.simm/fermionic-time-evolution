#ifndef IMPURITY_MODEL_THERMO_H
#define IMPURITY_MODEL_THERMO_H

#include "Hamiltonian.h"

/**
 * Utility functions for thermodynamic properties.
 */
class Thermo {
public:
    /**
     * Calculates the free energy of a system at a specific temperature.
     * @param hamiltonian the system's Hamiltonian
     * @param beta the inverse temperature
     */
    static double freeEnergy(const Hamiltonian *hamiltonian, double beta);

    /**
     * Calculates the entropy of a system at a specific temperature.
     * @param hamiltonian the system's Hamiltonian
     * @param beta the inverse temperature
     */
    static double entropy(const Hamiltonian *hamiltonian, double beta);

    /**
     * Calculates the entropy density of a system for a temperature range.
     * @param H the system's Hamiltonian
     * @param beta the range of inverse temperatures
     */
    static std::vector<double> entropyDensity(const Hamiltonian *H, std::vector<double> beta);
};

#endif //IMPURITY_MODEL_THERMO_H
