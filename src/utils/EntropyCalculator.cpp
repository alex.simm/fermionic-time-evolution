#include "EntropyCalculator.h"
#include "../utils.h"
#include <vector>
#include <bitset>

using namespace std;
using namespace arma;

EntropyCalculator::EntropyCalculator(const Basis &fullBasis, std::set<uint> sitesA)
        : fullBasis(fullBasis), sitesA(sitesA), sitesB(Utils::difference(fullBasis.getSites(), sitesA)),
          basisA(sitesA.size(), -1), basisB(sitesB.size(), -1),
          stateMatrix(basisA.dim(), basisB.dim()) {

    stateMatrix.fill(-1);

    vector<uint> vA(sitesA.begin(), sitesA.end());
    vector<uint> vB(sitesB.begin(), sitesB.end());
    for (State stateA : basisA.getStates()) {
        for (State stateB : basisB.getStates()) {
            State combined = State::combineStates(stateA, vA, stateB, vB);
            if (fullBasis.getStateIndex(combined) >= 0)
                stateMatrix(basisA.getStateIndex(stateA), basisB.getStateIndex(stateB))
                        = fullBasis.getStateIndex(combined);
        }
    }
}

EntropyCalculator::EntropyCalculator(const EntropyCalculator &e)
        : fullBasis(e.fullBasis), basisA(e.basisA), basisB(e.basisB) {
    this->operator=(e);
}

EntropyCalculator &EntropyCalculator::operator=(const EntropyCalculator &e) {
    if (&e != this) {
        fullBasis = e.fullBasis;
        basisA = e.basisA;
        basisB = e.basisB;
        sitesA = e.sitesA;
        sitesB = e.sitesB;
        stateMatrix = e.stateMatrix;
    }

    return *this;
}

std::set<uint> EntropyCalculator::getSitesA() const {
    return sitesA;
}

std::set<uint> EntropyCalculator::getSitesB() const {
    return sitesB;
}

arma::cx_mat EntropyCalculator::densityMatrix(const arma::cx_vec &psi) {
    return kron(psi, psi.t());
}

arma::cx_mat EntropyCalculator::partialTrace(const cx_vec &psi) const {
    if (psi.size() != fullBasis.dim())
        throw runtime_error("incompatible dimensions in entropy calculation: " + to_string(psi.size())
                            + " != " + to_string(fullBasis.dim()));

    cx_mat reducedRho(basisA.dim(), basisA.dim());
    for (State stateI : basisA.getStates()) {
        uint i = basisA.getStateIndex(stateI);
        for (State stateJ : basisA.getStates()) {
            uint j = basisA.getStateIndex(stateJ);

            reducedRho(i, j) = 0;
            for (uint k = 0; k < basisB.dim(); k++) {
                int idx_ik = stateMatrix(i, k);
                int idx_jk = stateMatrix(j, k);
                if (idx_ik >= 0 && idx_jk >= 0)
                    reducedRho(i, j) += psi[idx_ik] * conj(psi[idx_jk]);
            }
        }
    }

    return reducedRho;
}

double EntropyCalculator::entropy(const cx_mat &rho, bool diagonalise) {
    if (rho.n_elem == 1)
        return -(rho(0, 0) * log(rho(0, 0))).real();

    // entropy
    if (diagonalise) {
        vec eigenvalues(rho.n_rows, fill::zeros);
        bool success = eig_sym(eigenvalues, rho);
        if (!success)
            throw runtime_error(
                    "Could not diagonalise density matrix (" + to_string(rho.n_rows) + "x"
                    + to_string(rho.n_cols) + ")");
        //cout << "eigenvalues: "<< eigenvalues << endl;

        return -accu(eigenvalues.transform([](double d) {
            return (!isnan(d) && fabs(d) > ENTROPY_EIGENVALUE_CUTOFF) ? (d * log(d)) : 0;
        }));
    } else {
        return -trace(rho * logmat(rho)).real();
    }
}
