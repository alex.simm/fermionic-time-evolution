#include "StorageFile.h"
#include <fstream>
#include <sstream>
#include <iomanip>
#include "../utils.h"

using namespace std;
using namespace arma;

//TODO: function to test if file is accessible / writable
StorageFile::StorageFile(filesystem::path filename) : filename(filename), columns(0), columnNames(0) {
}

StorageFile::StorageFile(const StorageFile &s) {
    this->operator=(s);
}

StorageFile &StorageFile::operator=(const StorageFile &s) {
    if (&s != this) {
        filename = s.filename;
        columns = s.columns;
        columnNames = s.columnNames;
    }

    return *this;
}

std::filesystem::path StorageFile::getPath() const {
    return filename;
}

bool StorageFile::exists() const {
    return filesystem::exists(filename);
}

uint StorageFile::numColumns() const {
    return columns.size();
}

void StorageFile::addColumn(const vector<double> &column, const string &name) {
    columns.push_back(column);
    columnNames.push_back(name);
}

void StorageFile::addColumn(const vec &column, const string &name) {
    addColumn(Utils::convert<vec, double>(column), name);
}

void StorageFile::removeColumn(uint index) {
    columns.erase(columns.begin() + index);
}

void StorageFile::setColumn(uint index, const vector<double> &column) {
    columns[index] = column;
}

void StorageFile::setColumn(string name, const vector<double> &column) {
    auto found = find(columnNames.begin(), columnNames.end(), name);
    if (found == columnNames.end()) {
        addColumn(column, name);
    } else {
        uint index = std::distance(columnNames.begin(), found);
        columns[index] = column;
    }
}

bool StorageFile::hasColumn(string name) {
    return find(columnNames.begin(), columnNames.end(), name) != columnNames.end();
}

vector<double> StorageFile::getColumn(uint index) const {
    return *(columns.begin() + index);
}

vector<double> StorageFile::getColumn(string name) const {
    auto found = find(columnNames.begin(), columnNames.end(), name);
    if (found == columnNames.end()) {
        return vector<double>();
    } else {
        uint index = std::distance(columnNames.begin(), found);
        return columns[index];
    }
}

string StorageFile::getColumnName(uint index) const {
    return *(columnNames.begin() + index);
}

vector<vector<double>> StorageFile::getColumns() const {
    return columns;
}

vector<string> StorageFile::getColumnNames() const {
    return columnNames;
}

void StorageFile::setColumns(const vector<vector<double>> &cols, const vector<string> &names) {
    this->columns = cols;
    this->columnNames = names;
}

void StorageFile::clear() {
    columns.clear();
    columnNames.clear();
}

void StorageFile::deleteFile() {
    remove(filename.c_str());
}

void StorageFile::save() {
    ofstream out;
    out.open(filename, ofstream::trunc);
    out << setprecision(10);

    // write column names
    for (int j = 0; j < columns.size(); ++j) {
        out << columnNames[j];
        if (j != columns.size() - 1) out << DELIMITER;
    }
    out << endl;

    // write data
    if (!columns.empty()) {
        for (int i = 0; i < columns[0].size(); ++i) {
            for (int j = 0; j < columns.size(); ++j) {
                out << columns[j][i];
                if (j != columns.size() - 1) out << DELIMITER;
            }
            out << endl;
        }
    }

    out.flush();
    out.close();
}

void StorageFile::load() {
    clear();

    ifstream in(filename);
    if (!in.is_open())
        throw runtime_error("Could not open file");

    // read column names
    string line, colname;
    if (in.good()) {
        getline(in, line);
        stringstream ss(line);

        // Extract each column name
        while (getline(ss, colname, DELIMITER)) {
            // Initialize column data vectors
            columnNames.push_back(colname);
            columns.push_back(vector<double>{});
        }
    }

    // read data
    string value;
    while (getline(in, line)) {
        stringstream ss(line);

        int colIdx = 0;
        // extract values from line
        while (getline(ss, value, ',')) {
            columns[colIdx].push_back(toDouble(value));
            if (ss.peek() == DELIMITER)
                ss.ignore();

            colIdx++;
        }
    }

    in.close();
}

void StorageFile::combineFilesInclude(std::vector<std::pair<std::string, std::vector<uint>>> inputFiles,
                                      std::string outputFile) {
    StorageFile dst(outputFile);

    for (pair<string, vector<uint>> input : inputFiles) {
        StorageFile src(input.first);
        src.load();

        for (uint i : input.second) {
            dst.addColumn(src.getColumn(i), src.getColumnName(i));
        }
    }

    dst.save();
}

void StorageFile::combineFilesExclude(std::vector<std::pair<std::string, std::vector<uint>>> inputFiles,
                                      std::string outputFile) {
    StorageFile dst(outputFile);

    for (pair<string, vector<uint>> input : inputFiles) {
        StorageFile src(input.first);
        src.load();

        for (uint i = 0; i < src.numColumns(); i++) {
            if (!Utils::contains(input.second, i))
                dst.addColumn(src.getColumn(i), src.getColumnName(i));
        }
    }

    dst.save();
}

double StorageFile::toDouble(std::string s) const {
    std::transform(s.begin(), s.end(), s.begin(), [](unsigned char c) { return std::tolower(c); });

    if (s == "inf") {
        return std::numeric_limits<double>::infinity();
    } else if (s == "-inf") {
        return -std::numeric_limits<double>::infinity();
    } else if (s == "nan") {
        return std::numeric_limits<double>::quiet_NaN();
    } else if (s == "-nan") {
        return -std::numeric_limits<double>::quiet_NaN();
    } else {
        return std::atof(s.c_str());
    }
}