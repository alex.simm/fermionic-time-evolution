#include "GreensFunctionCalculator.h"
#include "../utils.h"
#include "../Log.h"

using namespace std;
using namespace arma;

GreensFunctionCalculator::GreensFunctionCalculator(HamiltonianBlock *H, HamiltonianBlock *Hplus,
                                                   HamiltonianBlock *Hminus, double eta,
                                                   double groundStateEnergyTolerance)
        : H(H), Hplus(Hplus), Hminus(Hminus), eta(eta), groundStateEnergyTolerance(groundStateEnergyTolerance) {
    if ((Hplus != nullptr && H->getBasis().getN() != Hplus->getBasis().getN())
        || (Hminus != nullptr && H->getBasis().getN() != Hminus->getBasis().getN()))
        throw runtime_error("Hamiltonians need to have the same number of sites");

    // creation and annihilation operators for each site
    for (uint site = 0; site < H->getBasis().getN(); site++) {
        if (Hplus != nullptr)
            annihilationOps.push_back(annihilationMatrix(Hplus->getBasis(), H->getBasis(), site));
        if (Hminus != nullptr)
            creationOps.push_back(creationMatrix(Hminus->getBasis(), H->getBasis(), site));
    }

    // find all ground states
    const vec &evals = H->getEigenvalues();
    const cx_mat &evecs = H->getEigenvectors();
    gsEnergies.push_back(evals[0]);
    groundStates.push_back(evecs.col(0));

    uint idx = 0;
    while (idx < evals.size() && (evals[idx] - gsEnergies[0]) < groundStateEnergyTolerance) {
        gsEnergies.push_back(evals[idx]);
        groundStates.push_back(evecs.col(idx));
        idx++;
    }
}

GreensFunctionCalculator::~GreensFunctionCalculator() {
    for (auto p : creationOps) {
        delete p;
    }
    creationOps.clear();
    for (auto p:annihilationOps) {
        delete p;
    }
    annihilationOps.clear();
}

uint GreensFunctionCalculator::numGroundStates() const {
    return gsEnergies.size();
}

mat *GreensFunctionCalculator::annihilationMatrix(const Basis &src, const Basis &dst, uint site) {
    const vector<State> &statesSrc = src.getStates();
    const vector<State> &statesDst = dst.getStates();

    mat *c = new mat(statesDst.size(), statesSrc.size(), fill::zeros);

    for (const State &state : statesSrc) {
        if (state.isSiteOccupied(site)) {
            int idx = src.getStateIndex(state);
            State state2(state);
            state2.annihilate(site);

            if (!state2.isZero())
                (*c)(dst.getStateIndex(state2), idx) = (double) state2.getCoefficient();
        }
    }

    return c;
}

mat *GreensFunctionCalculator::creationMatrix(const Basis &src, const Basis &dst, uint site) {
    const vector<State> &statesSrc = src.getStates();
    const vector<State> &statesDst = dst.getStates();

    mat *c = new mat(statesDst.size(), statesSrc.size(), fill::zeros);

    for (const State &state : statesSrc) {
        if (!state.isSiteOccupied(site)) {
            int idx = src.getStateIndex(state);
            State state2(state);
            state2.create(site);

            if (!state2.isZero())
                (*c)(dst.getStateIndex(state2), idx) = (double) state2.getCoefficient();
        }
    }

    return c;
}

double GreensFunctionCalculator::spectralFunction(double omega, uint site) const {
    double sum = 0;

    if (!creationOps.empty()) {
        mat *op = creationOps[site];
        const vec &evalsMinus = Hminus->getEigenvalues();
        const cx_mat evecsMinus = Hminus->getEigenvectors();

        // iterate all ground states and all states with one particle less
#pragma omp parallel for
        for (int m = 0; m < evalsMinus.size(); ++m) {
            double privateSum = 0;
            cx_vec created = (*op) * evecsMinus.col(m);

            for (uint n = 0; n < gsEnergies.size(); n++) {
                double product = norm(cdot(groundStates[n], created));
                privateSum += product * Utils::delta(omega - gsEnergies[n] + evalsMinus[n], eta);
            }

#pragma omp critical
            sum += privateSum;
        }
    }

    if (!annihilationOps.empty()) {
        mat *op = annihilationOps[site];
        const vec &evalsPlus = Hplus->getEigenvalues();
        const cx_mat evecsPlus = Hplus->getEigenvectors();

        // iterate all ground states and all states with one particle more
#pragma omp parallel for
        for (int m = 0; m < evalsPlus.size(); ++m) {
            double privateSum = 0;
            cx_vec annihilated = (*op) * evecsPlus.col(m);

            for (uint n = 0; n < gsEnergies.size(); n++) {
                double product = norm(cdot(groundStates[n], annihilated));
                privateSum += product * Utils::delta(omega + gsEnergies[n] - evalsPlus[n], eta);
            }

#pragma omp critical
            sum += privateSum;
        }
    }

    return sum / (gsEnergies.size() * H->getBasis().getN());
}

vec GreensFunctionCalculator::spectralFunction(vec omegas, uint site) const {
    vec sum(omegas.size(), fill::zeros);

    if (!creationOps.empty()) {
        mat *op = creationOps[site];
        const vec &evalsMinus = Hminus->getEigenvalues();
        const cx_mat evecsMinus = Hminus->getEigenvectors();

        // iterate all ground states (n) and all states with one particle less (m)
#pragma omp parallel for
        for (int m = 0; m < evalsMinus.size(); ++m) {
            vec privateSum(omegas.size(), fill::zeros);

            cx_vec created = (*op) * evecsMinus.col(m);
            for (uint n = 0; n < gsEnergies.size(); n++) {
                double product = norm(cdot(groundStates[n], created));
                double Ediff = gsEnergies[n] - evalsMinus[m];

                if (product != 0) {
                    for (int i = 0; i < omegas.size(); ++i) {
                        double delta = Utils::delta(omegas[i] - Ediff, eta);
                        sum[i] += product * delta;
                    }
                }
            }

#pragma omp critical
            sum += privateSum;
        }
    }

    if (!annihilationOps.empty()) {
        mat *op = annihilationOps[site];
        const vec &evalsPlus = Hplus->getEigenvalues();
        const cx_mat evecsPlus = Hplus->getEigenvectors();

        // iterate all ground states (n) and all states with one particle more (m)
#pragma omp parallel for
        for (int m = 0; m < evalsPlus.size(); ++m) {
            vec privateSum(omegas.size(), fill::zeros);

            cx_vec annihilated = (*op) * evecsPlus.col(m);
            for (uint n = 0; n < gsEnergies.size(); n++) {
                double product = norm(cdot(groundStates[n], annihilated));
                double Ediff = gsEnergies[n] - evalsPlus[m];

                if (product != 0) {
                    for (int i = 0; i < omegas.size(); ++i) {
                        double delta = Utils::delta(omegas[i] + Ediff, eta);
                        sum[i] += product * delta;
                    }
                }
            }

#pragma omp critical
            sum += privateSum;
        }
    }

    return sum / (gsEnergies.size() * H->getBasis().getN());
}

double GreensFunctionCalculator::densityOfStates(double omega) {
    timer.start();

    double trace = 0;
    for (int i = 0; i < H->getBasis().getN(); ++i) {
        Log::debug() << "spectral function of site " << i << "\t" << timer.toString(H->getBasis().getN()) << endl;
        trace += spectralFunction(omega, i);
        timer.nextRound();
    }
    return trace;
}

vec GreensFunctionCalculator::densityOfStates(vec omegas) {
    timer.start();

    vec trace(omegas.size(), fill::zeros);
    for (int i = 0; i < H->getBasis().getN(); ++i) {
        Log::debug() << "spectral function of site " << i << "\t" << timer.toString(H->getBasis().getN()) << endl;
        trace += spectralFunction(omegas, i);
        timer.nextRound();
    }
    return trace;
}
