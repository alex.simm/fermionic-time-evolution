#ifndef IMPURITY_MODEL_HISTOGRAM_H
#define IMPURITY_MODEL_HISTOGRAM_H

#include <vector>
#include <numeric>
#include <algorithm>
#include <cmath>

template<typename T>
class Histogram {
public:
    /**
     * @param n_bins: the number of bins in each histogram dimension.
     * @param n_dims_data: the number of dimensions the data has (e.g. 3 for vector field).
     * @param limits: the minimum/maximum data values to consider for the histogram.
     */
    explicit Histogram(std::vector<size_t> n_bins, size_t n_dims_data, std::vector<std::pair<T, T>> limits)
            : m_n_bins(n_bins), m_n_dims_data(n_dims_data), m_limits(limits) {
        if (n_bins.size() != limits.size()) {
            throw std::invalid_argument("Argument for number of bins and limits do "
                                        "not have same number of dimensions!");
        }
        m_bin_sizes = calc_bin_sizes(limits, n_bins);
        size_t n_bins_total = m_n_dims_data * std::accumulate(std::begin(n_bins), std::end(n_bins), 1,
                                                              std::multiplies<size_t>());
        m_hist = std::vector<T>(n_bins_total);
    };

    /**
     * Get the number of bins for each dimension.
     */
    std::vector<size_t> getNumBins() const {
        return m_n_bins;
    }

    /**
     * Get the histogram data.
     */
    std::vector<T> getHistogram() const {
        return m_hist;
    }

    /**
     * Get the ranges (min, max) for each dimension.
     */
    std::vector<std::pair<T, T>> getLimits() const {
        return m_limits;
    }

    /**
     * Get the bin sizes.
     */
    std::vector<T> getBinSizes() const {
        return m_bin_sizes;
    }

    /**
     * Add data to the histogram.
     * @param data: vector of single data value with type T. The size of the given vector has to match the number of
     * dimensions of the histogram.
     */
    void update(std::vector<T> const &data) {
        if (check_limits(data, m_limits)) {
            std::vector<T> weights(m_n_dims_data, static_cast<T>(1.0));
            update(data, weights);
        }
    }

    /**
     * Add data to the histogram.
     * @param data: vector of single data value with type T. The size of the given vector has to match the number of
     * dimensions of the histogram.
     * @param weights: m_n_dims_data dimensional weights.
     */
    void update(std::vector<T> const &data, std::vector<T> const &weights) {
        if (check_limits(data, m_limits)) {
            std::vector<size_t> index;
            for (size_t dim = 0; dim < m_n_bins.size(); ++dim) {
                index.push_back(calculate_bin_index(data[dim], m_bin_sizes[dim],
                                                    m_limits[dim].first));
            }
            size_t flat_index = m_n_dims_data * ravel_index(index, m_n_bins);
            if (weights.size() != m_n_dims_data)
                throw std::invalid_argument("Wrong dimensions of given weights!");
            for (size_t ind = 0; ind < m_n_dims_data; ++ind) {
                m_hist[flat_index + ind] += static_cast<T>(weights[ind]);
            }
        }
    }

    /**
     * Histogram normalization.
     */
    void normalise() {
        T tot_count = std::accumulate(m_hist.begin(), m_hist.end(), static_cast<T>(0.0));
        std::transform(m_hist.begin(), m_hist.end(), m_hist.begin(), [tot_count](T v) { return v / tot_count; });
    }

private:
    // Number of bins for each dimension.
    std::vector<size_t> m_n_bins;
    // Number of dimensions for a single data point.
    size_t m_n_dims_data;
    // Min and max values for each dimension.
    std::vector<std::pair<T, T>> m_limits;
    // Bin sizes for each dimension.
    std::vector<T> m_bin_sizes;
    // Flat histogram data.
    std::vector<T> m_hist;

    /**
     * Calculate the bin sizes.
     * @param limits: containts min/max values for each dimension.
     * @param nbins: number of bins for each dimension.
     * @return The bin sizes for each dimension.
     */
    std::vector<T> calc_bin_sizes(std::vector<std::pair<T, T>> const &limits,
                                  std::vector<size_t> const &n_bins) {
        std::vector<T> tmp;
        for (size_t ind = 0; ind < limits.size(); ++ind) {
            tmp.push_back((limits[ind].second - limits[ind].first) / n_bins[ind]);
        }
        return tmp;
    }

    /**
     * Checks if data is within limits.
     * @param data: data value to check.
     * @param limits: the min/max values.
     */
    bool inline check_limits(std::vector<T> const &data, std::vector<std::pair<T, T>> limits) {
        if (data.size() != limits.size()) {
            throw std::invalid_argument("Dimension of data and limits not the same!");
        }
        bool res = true;
        for (size_t i = 0; i < data.size(); ++i) {
            if (data[i] < limits[i].first or data[i] > limits[i].second)
                res = false;
        }
        return res;
    }

    size_t calculate_bin_index(double value, double bin_size) {
        return std::floor(value / bin_size);
    }

    size_t calculate_bin_index(double value, double bin_size, double offset) {
        return (calculate_bin_index(value, bin_size) - std::floor(offset / bin_size));
    }
};

#endif //IMPURITY_MODEL_HISTOGRAM_H
