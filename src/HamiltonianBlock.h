#ifndef IMPURITY_MODEL_HAMILTONIANBLOCK_H
#define IMPURITY_MODEL_HAMILTONIANBLOCK_H

#include <armadillo>
#include <set>
#include "State.h"
#include "Hamiltonian.h"
#include "Basis.h"
#include "hamiltonians/HamiltonianTerm.h"
#include "hamiltonians/TermCallback.h"

class HamiltonianBlock : public Hamiltonian, TermCallback {
public:
    /** Constructs a Hamiltonian block with N sites and Q particles without impurity. */
    HamiltonianBlock(uint N, uint Q, std::vector<HamiltonianTerm *> terms, bool sparse = false);

    /** Constructs a Hamiltonian block with N sites plus one impurity and Q particles. */
    HamiltonianBlock(uint N, uint Q, std::vector<HamiltonianTerm *> terms, double impurityEnergy, double V,
                     bool sparse = false);

    HamiltonianBlock(const HamiltonianBlock &h);

    virtual ~HamiltonianBlock() = default;

    HamiltonianBlock &operator=(const HamiltonianBlock &h);

    const Basis &getBasis() const override;

    bool isUsingSparseMatrix() const override;

    double getSparseness() const override;

    arma::cx_mat asMatrix() const override;

    arma::sp_cx_mat asSparseMatrix() const override;

    /** Returns the matrix representation of this Hamiltonian without creating a copy. */
    const arma::cx_mat &matrix() const;

    /**
     * Returns the sparse matrix representation of this Hamiltonian, if it is using a sparse matrix,
     * without creating a copy.
     */
    const arma::sp_cx_mat &sparseMatrix() const;

    bool isDiagonalised() const override;

    void diagonalise(bool includeEigenvectors = true, bool onlyGroundState = false) override;

    const arma::vec &getEigenvalues() const override;

    const arma::cx_mat &getEigenvectors() const override;

    void updateImpurityEnergy(double E) override;

    void updateHoppingPotential(double V) override;

    // ==================== TermCallback interface =====================
    /**
     * Adds a real term to the Hamiltonian if both states exist.
     */
    void addEntry(std::complex<double> entry, const State &state1, const State &state2, ulong state1Index) override;

    /**
     * Adds a real term to the Hamiltonian if both states exist.
     */
    void addEntry(std::complex<double> entry, const State &state1, const State &state2, ulong state1Index,
                  ulong state2Index) override;

    void updateMatrix(bool updateTerms = false) override;

private:
    const uint MIN_DIAGONALISATION_EIGENVALUES = 3;
    const uint MIN_DIAGONALISATION_EIGENVECTORS = 3;

    /** Whether the Hamiltonian only contains real values. */
    bool realMatrix;
    /** Whether the matrix is sparse. */
    bool sparse;
    /* Basis of the Hilbert space. */
    Basis basis;
    /** Matrix representation */
    arma::cx_mat H;
    arma::sp_cx_mat Hsparse;
    /** Eigenvalues and eigenvectors */
    arma::vec eigenvalues;
    arma::cx_mat eigenvectors;

    /**
     * Adds on-site energy for the impurity and hopping terms from all band sites to the impurity and vice versa to
     * the Hamiltonian. This clears the diagonalisation status and all eigenvalues and eigenvectors.
     */
    void addImpurityTerms(double Eimp, double V);

    /**
     * Adds on-site energy for the impurity to the Hamiltonian. This clears the diagonalisation status and all
     * eigenvalues and eigenvectors.
     */
    void addImpurityOnsiteTerms(double Eimp);

    /**
     * Adds hopping terms from all band sites to the impurity and vice versa to the Hamiltonian. This clears the
     * diagonalisation status and all eigenvalues and eigenvectors.
     */
    void addImpurityHoppingTerms(double V);

    /**
     * Finds the highest or lowest eigenvalues of the sparse Hamiltonian.
     */
    arma::vec findSparseEigenvalues(uint num, bool highest);

    /**
     * Finds the eigenvector of the sparse Hamiltonian with the highest or lowest eigenvalue. Returns both the
     * eigenvalue and the eigenvector.
     */
    std::pair<double, arma::cx_vec> findSparseEigenvector(bool highest);
};

#endif //IMPURITY_MODEL_HAMILTONIANBLOCK_H
