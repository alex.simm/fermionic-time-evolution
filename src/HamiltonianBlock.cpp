#include "HamiltonianBlock.h"
#include <stdexcept>
#include "utils.h"
#include "Log.h"

using namespace std;
using namespace arma;

HamiltonianBlock::HamiltonianBlock(uint N, uint Q, vector<HamiltonianTerm *> terms, bool sparse)
        : Hamiltonian(N, terms), basis(N, Q), sparse(sparse), realMatrix(true) {
    for (HamiltonianTerm *term : terms) {
        if (!term->isReal())
            realMatrix = false;
    }

    updateMatrix(false);
}

HamiltonianBlock::HamiltonianBlock(uint N, uint Q, vector<HamiltonianTerm *> terms, double impurityEnergy, double V,
                                   bool sparse)
        : Hamiltonian(N, terms, impurityEnergy, V), basis(N + 1, Q), sparse(sparse), realMatrix(true) {
    for (HamiltonianTerm *term : terms) {
        if (!term->isReal())
            realMatrix = false;
    }

    updateMatrix(false);
}

HamiltonianBlock::HamiltonianBlock(const HamiltonianBlock &h) : Hamiltonian(h), basis(h.basis) {
    this->operator=(h);
}

HamiltonianBlock &HamiltonianBlock::operator=(const HamiltonianBlock &h) {
    if (this != &h) {
        Hamiltonian::operator=(h);
        realMatrix = h.realMatrix;
        sparse = h.sparse;
        basis = h.basis;
        H = h.H;
        Hsparse = h.Hsparse;
        eigenvalues = h.eigenvalues;
        eigenvectors = h.eigenvectors;
    }

    return *this;
}

const Basis &HamiltonianBlock::getBasis() const {
    return basis;
}

bool HamiltonianBlock::isUsingSparseMatrix() const {
    return sparse;
}

double HamiltonianBlock::getSparseness() const {
    const uint dim = basis.dim();

    int numElements = 0;
    if (sparse) {
        numElements = Hsparse.n_nonzero;
    } else {
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                if (abs(H(i, j)) > 1e-10)
                    numElements++;
            }
        }
    }
    return (double) numElements / (dim * dim);
}

cx_mat HamiltonianBlock::asMatrix() const {
    return H;
}

arma::sp_cx_mat HamiltonianBlock::asSparseMatrix() const {
    return Hsparse;
}

const cx_mat &HamiltonianBlock::matrix() const {
    return H;
}

const arma::sp_cx_mat &HamiltonianBlock::sparseMatrix() const {
    return Hsparse;
}

bool HamiltonianBlock::isDiagonalised() const {
    return eigenvalues.size() > 0;
}

void HamiltonianBlock::diagonalise(bool includeEigenvectors, bool onlyGroundState) {
    if (sparse) {
        if (onlyGroundState) {
            eigenvalues.resize(1);

            if (includeEigenvectors) {
                eigenvectors.resize(Hsparse.n_rows, 1);
                pair<double, cx_vec> result = findSparseEigenvector(false);

                eigenvalues[0] = result.first;
                eigenvectors.col(0) = result.second;
            } else {
                eigenvalues[0] = findSparseEigenvalues(1, false)[0];
            }
        } else {
            if (includeEigenvectors) {
                throw new runtime_error("For sparse matrices only the ground state can be obtained");
            }

            // find all eigenvalues
            eigenvalues.resize(Hsparse.n_rows);
            eigenvalues.fill(0);

            uint smallerHalf = Hsparse.n_rows / 2;
            uint largerHalf = Hsparse.n_rows - smallerHalf;
            eigenvalues.subvec(0, largerHalf - 1) = findSparseEigenvalues(largerHalf, true);
            eigenvalues.subvec(largerHalf, largerHalf + smallerHalf - 1) = findSparseEigenvalues(smallerHalf, false);
            eigenvalues = sort(eigenvalues);
        }
    } else {
        bool success;
        if (includeEigenvectors)
            success = eig_sym(eigenvalues, eigenvectors, H);
        else
            success = eig_sym(eigenvalues, H);
        if (!success)
            throw runtime_error(
                    "Could not diagonalise Hamiltonian (" + to_string(H.n_rows) + "x"
                    + to_string(H.n_cols) + ")");
    }

    // normalise eigenvectors
    if (includeEigenvectors) {
        for (uint i = 0; i < eigenvectors.n_cols; i++) {
            eigenvectors.col(i) = normalise(eigenvectors.col(i));
        }
    }
}

vec HamiltonianBlock::findSparseEigenvalues(uint num, bool highest) {
    uint numValues = min(num + 1, 3u);
    Log::debug() << "find sparse eigenvalues (" << (realMatrix ? "real" : "complex") << ": " << numValues << endl;

    vec evalsReal;
    bool success;
    if (realMatrix) {
        evalsReal.resize(numValues);
        sp_mat Hreal = real(Hsparse);
        success = eigs_sym(evalsReal, Hreal, numValues, highest ? "la" : "sa");
    } else {
        cx_vec evals(numValues);
        success = eigs_gen(evals, Hsparse, numValues, highest ? "lr" : "sr");
        if (success)
            evalsReal = real(evals);
    }

    if (!success)
        throw runtime_error("Could not diagonalise Hamiltonian (" + to_string(Hsparse.n_rows) + "x"
                            + to_string(Hsparse.n_cols) + ")");

    return highest ? evalsReal.tail(num) : evalsReal.head(num);
}

pair<double, cx_vec> HamiltonianBlock::findSparseEigenvector(bool highest) {
    /* For large matrices the diagonalisation seems to fail if too few eigenvectors are chosen.
     * Increase the number of eigenvectors with the matrix size. */
    uint num = MIN_DIAGONALISATION_EIGENVECTORS + uint(Hsparse.n_rows / 10000);
    Log::debug() << "find sparse eigenvectors (" << (realMatrix ? "real" : "complex") << "): "
                 << Hsparse.n_rows << "x" << num << endl;

    double eval;
    cx_vec evec;
    bool success;
    if (realMatrix) {
        mat evecs(Hsparse.n_rows, num);
        vec evals(num);

        success = eigs_sym(evals, evecs, real(Hsparse), num, highest ? "la" : "sa");
        if (success) {
            eval = evals[0];
            evec = cx_vec(evecs.col(0), vec(evecs.n_rows, fill::zeros));
        }
    } else {
        cx_mat evecs(Hsparse.n_rows, num);
        cx_vec evals(num);

        success = eigs_gen(evals, evecs, Hsparse, num, highest ? "lr" : "sr");
        if (success) {
            eval = real(evals[0]);
            evec = evecs.col(0);
        }
    }

    if (!success)
        throw runtime_error(
                "Could not diagonalise Hamiltonian (" + to_string(Hsparse.n_rows) + "x"
                + to_string(Hsparse.n_cols) + ")");

    return pair<double, cx_vec>(eval, evec);
}

const vec &HamiltonianBlock::getEigenvalues() const {
    if (!isDiagonalised())
        throw runtime_error("Hamiltonian is not diagonalised");
    return eigenvalues;
}

const cx_mat &HamiltonianBlock::getEigenvectors() const {
    if (!isDiagonalised())
        throw runtime_error("Hamiltonian is not diagonalised");
    return eigenvectors;
}

void HamiltonianBlock::updateMatrix(bool updateTerms) {
    if (updateTerms) {
        for (HamiltonianTerm *term : terms) {
            term->update();
        }
    }

    // create a new matrix
    if (sparse) {
        Hsparse = sp_cx_mat(basis.dim(), basis.dim());
        H = cx_mat(0, 0);
    } else {
        H = cx_mat(basis.dim(), basis.dim(), fill::zeros);
        Hsparse = sp_cx_mat(0, 0);
    }

    // fill it by calling all terms
    for (HamiltonianTerm *term : terms) {
        term->addEntries(this, basis, bandSites);
    }

    if (hasImpurity())
        addImpurityTerms(impurityEnergy, impurityHopping);
}

void HamiltonianBlock::updateImpurityEnergy(double E) {
    addImpurityOnsiteTerms(-impurityEnergy);
    addImpurityOnsiteTerms(E);
    impurityEnergy = E;
}

void HamiltonianBlock::updateHoppingPotential(double V) {
    addImpurityHoppingTerms(-impurityHopping);
    addImpurityHoppingTerms(V);
    impurityHopping = V;
}

void HamiltonianBlock::addImpurityTerms(double Eimp, double V) {
    addImpurityOnsiteTerms(Eimp);
    addImpurityHoppingTerms(V);
}

void HamiltonianBlock::addImpurityOnsiteTerms(double Eimp) {
    if (!hasImpurity())
        throw runtime_error("cannot add impurity on-site terms: no impurity");

    if (Eimp != 0) {
        for (State state1 : basis.getStates()) {
            int idx1 = basis.getStateIndex(state1);

            if (state1.isSiteOccupied(impurityIndex))
                addEntry(Eimp, state1, state1, idx1, idx1);
        }

        eigenvalues.clear();
        eigenvectors.clear();
    }
}

void HamiltonianBlock::addImpurityHoppingTerms(double V) {
    if (!hasImpurity())
        throw runtime_error("cannot add hopping terms: no impurity");

    if (V != 0) {
        const double hoppingCoef = V / sqrt(bandSites.size());

        State state2(basis.getN(), 0);
        for (State state1 : basis.getStates()) {
            int idx1 = basis.getStateIndex(state1);

            for (uint i : bandSites) {
                if (i != impurityIndex) {
                    // hopping from impurity
                    state1.applyTerm(i, impurityIndex, state2);
                    if (!state2.isZero())
                        addEntry(hoppingCoef, state1, state2, idx1);

                    // hopping to impurity
                    state1.applyTerm(impurityIndex, i, state2);
                    if (!state2.isZero())
                        addEntry(hoppingCoef, state1, state2, idx1);
                }
            }
        }

        eigenvalues.clear();
        eigenvectors.clear();
    }
}

void HamiltonianBlock::addEntry(complex<double> entry, const State &state1, const State &state2, ulong state1Index) {
    if (!state1.isZero() && !state2.isZero()) {
        ulong d = basis.dim();

        if (state1Index < 0 || state1Index >= d)
            throw runtime_error("Illegal state index occured: " + to_string(state1Index));

        int state2Index = basis.getStateIndex(state2);
        if (state2Index < 0 || state2Index >= d)
            throw runtime_error("Illegal state index occured: " + to_string(state2Index));

        if (sparse)
            Hsparse.at(state2Index, state1Index) += entry * (double) state2.getCoefficient();
        else
            H.at(state2Index, state1Index) += entry * (double) state2.getCoefficient();
    }
}

void HamiltonianBlock::addEntry(complex<double> entry, const State &state1, const State &state2, ulong state1Index,
                                ulong state2Index) {
    if (!state1.isZero() && !state2.isZero()) {
        ulong d = basis.dim();
        if (state1Index < 0 || state1Index >= d)
            throw runtime_error("Illegal state index occured: " + to_string(state1Index));
        if (state2Index < 0 || state2Index >= d)
            throw runtime_error("Illegal state index occured: " + to_string(state2Index));

        if (sparse)
            Hsparse.at(state2Index, state1Index) += entry * (double) state2.getCoefficient();
        else
            H.at(state2Index, state1Index) += entry * (double) state2.getCoefficient();
    }
}
