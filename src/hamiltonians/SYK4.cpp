#include "SYK4.h"
#include <iostream>
#include "../Log.h"

using namespace std;
using namespace arma;

SYK4::SYK4(uint N, double paramJ) : HamiltonianTerm(false), J(N, paramJ) {
}

SYK4::SYK4(const SYK4 &s) : HamiltonianTerm(s), J(s.J) {
    this->operator=(s);
}

SYK4 &SYK4::operator=(const SYK4 &s) {
    if (this != &s) {
        HamiltonianTerm::operator=(s);
        J = s.J;
    }

    return *this;
}

std::string SYK4::toString() const {
    return "SYK q=4 (J=" + to_string(J.getJ()) + ")";
}

void SYK4::update() {
    J = InteractionParameter(J.getN(), J.getJ());
}

const InteractionParameter &SYK4::getJ() const {
    return J;
}

void SYK4::addEntries(TermCallback *callback, const Basis &basis, set<uint> bandSites) {
    double scaling = 1.0 / pow(2.0 * bandSites.size(), 1.5);
    const vector<State> &states = basis.getStates();

    int idx = 0;
    State state2(basis.getN(), 0);
    for (const State &state1 : states) {
        if (basis.getStates().size() > 100 && basis.getN() > 12 && (idx++) % 100 == 0)
            Log::debug() << "\tstate " << idx << " / " << states.size() << endl;

        ulong state1Idx = basis.getStateIndex(state1);

        // main terms
        for (uint i : bandSites) {
            for (uint j = *bandSites.begin(); j < i; j++) {
                for (uint k : bandSites) {
                    for (uint l = *bandSites.begin(); l < k; l++) {
                        state1.applyTerm(i, j, k, l, state2);
                        if (!state2.isZero()) {
                            ulong state2Idx = basis.getStateIndex(state2);
                            callback->addEntry(J.get(i, j, k, l) * scaling, state1, state2, state1Idx, state2Idx);
                            callback->addEntry(-J.get(j, i, k, l) * scaling, state1, state2, state1Idx, state2Idx);
                            callback->addEntry(-J.get(i, j, l, k) * scaling, state1, state2, state1Idx, state2Idx);
                            callback->addEntry(J.get(j, i, l, k) * scaling, state1, state2, state1Idx, state2Idx);
                        }
                    }
                }
            }
        }

        // correction terms
        for (uint i : bandSites) {
            for (uint j : bandSites) {
                for (uint l : bandSites) {
                    state1.applyTerm(j, l, state2);
                    callback->addEntry(0.5 * J.get(i, j, i, l) * scaling, state1, state2, state1Idx);

                    state1.applyTerm(i, l, state2);
                    callback->addEntry(-0.5 * J.get(i, j, j, l) * scaling, state1, state2, state1Idx);
                }
            }
        }
        for (uint i : bandSites) {
            for (uint j : bandSites) {
                for (uint k : bandSites) {
                    state1.applyTerm(i, k, state2);
                    callback->addEntry(0.5 * J.get(i, j, k, j) * scaling, state1, state2, state1Idx);

                    state1.applyTerm(j, k, state2);
                    callback->addEntry(-0.5 * J.get(i, j, k, i) * scaling, state1, state2, state1Idx);
                }
            }
        }
        for (uint i : bandSites) {
            for (uint j : bandSites) {
                callback->addEntry(-0.25 * J.get(i, j, i, j) * scaling, state1, state1, state1Idx, state1Idx);
                callback->addEntry(0.25 * J.get(i, j, j, i) * scaling, state1, state1, state1Idx, state1Idx);
            }
        }
    }
}
