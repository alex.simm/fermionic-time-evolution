#ifndef INTERACTION_PARAMETER_H_
#define INTERACTION_PARAMETER_H_

#include <boost/multi_array.hpp>

class InteractionParameter {
public:
    InteractionParameter(uint N, double J);

    ~InteractionParameter() = default;

    InteractionParameter(const InteractionParameter &p);

    InteractionParameter &operator=(const InteractionParameter &p);

    uint getN() const;

    double getJ() const;

    std::complex<double> get(uint i, uint j, uint k, uint l) const;

protected:
    InteractionParameter() = default;

private:
    uint N;
    double J;
    boost::multi_array<std::complex<double>, 4> values;
};

#endif // INTERACTION_PARAMETER_H_
