#ifndef IMPURITY_MODEL_HOPPING_H
#define IMPURITY_MODEL_HOPPING_H

#include "HamiltonianTerm.h"

class Hopping : public HamiltonianTerm {
public:
    Hopping(double hoppingEnergy);

    Hopping(const Hopping &s);

    Hopping &operator=(const Hopping &h);

    ~Hopping() = default;

    void addEntries(TermCallback *callback, const Basis &basis, std::set<uint> bandSites) override;

    std::string toString() const override;

private:
    double hoppingEnergy;
};

#endif //IMPURITY_MODEL_HOPPING_H
