#include "HamiltonianTerm.h"

using namespace std;
using namespace arma;

HamiltonianTerm::HamiltonianTerm(bool real) : real(real) {
}

HamiltonianTerm::HamiltonianTerm(const HamiltonianTerm &h) {
    this->operator=(h);
}

HamiltonianTerm &HamiltonianTerm::operator=(const HamiltonianTerm &h) {
    if (&h != this) {
        real = h.real;
    }

    return *this;
}

bool HamiltonianTerm::isReal() const {
    return real;
}

void HamiltonianTerm::update() {
}
