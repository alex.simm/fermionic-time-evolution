#ifndef IMPURITY_MODEL_INTERACTION_H
#define IMPURITY_MODEL_INTERACTION_H

#include "HamiltonianTerm.h"

class InteractionTerm : public HamiltonianTerm {
public:
    InteractionTerm(double strength);

    InteractionTerm(const InteractionTerm &o);

    InteractionTerm &operator=(const InteractionTerm &o);

    ~InteractionTerm() = default;

    void addEntries(TermCallback *callback, const Basis &basis, std::set<uint> bandSites) override;

    std::string toString() const override;

    void setStrength(double value);

    const double &getStrength() const;

private:
    double strength;
};

#endif //IMPURITY_MODEL_INTERACTION_H
