#ifndef TERM_CALLBACK_H
#define TERM_CALLBACK_H

#include "../State.h"
#include <complex>

/**
 * Callback from HamiltonianTerm instances to the Hamiltonian that allows adding entries to the matrix.
 */
class TermCallback {
public:
    virtual void addEntry(std::complex<double> entry, const State &state1, const State &state2,
                          ulong state1Index) = 0;

    virtual void addEntry(std::complex<double> entry, const State &state1, const State &state2, ulong state1Index,
                          ulong state2Index) = 0;
};

#endif //TERM_CALLBACK_H
