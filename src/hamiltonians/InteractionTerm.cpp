#include "InteractionTerm.h"
#include "../utils.h"
#include <iostream>

using namespace std;
using namespace arma;

InteractionTerm::InteractionTerm(double strength) : HamiltonianTerm(true), strength(strength) {
}

InteractionTerm::InteractionTerm(const InteractionTerm &o) : HamiltonianTerm(o) {
    this->operator=(o);
}

InteractionTerm &InteractionTerm::operator=(const InteractionTerm &o) {
    if (&o != this) {
        HamiltonianTerm::operator=(o);
        strength = o.strength;
    }

    return *this;
}

string InteractionTerm::toString() const {
    stringstream s;
    s << "interaction strength=" << strength;
    return s.str();
}

const double &InteractionTerm::getStrength() const {
    return strength;
}

void InteractionTerm::setStrength(double value) {
    strength = value;
}

void InteractionTerm::addEntries(TermCallback *callback, const Basis &basis, set<uint> bandSites) {
    for (State state : basis.getStates()) {
        int stateIdx = basis.getStateIndex(state);

        for (uint i : bandSites) {
            for (uint j : bandSites) {
                if (i != j && state.isSiteOccupied(i) && state.isSiteOccupied(j)) {
                    callback->addEntry(strength, state, state, stateIdx, stateIdx);
                }
            }
        }
    }
}
