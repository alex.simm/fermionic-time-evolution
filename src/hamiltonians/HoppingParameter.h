#ifndef HOPPING_PARAMETER_H
#define HOPPING_PARAMETER_H

#include <complex>
#include <boost/multi_array.hpp>
#include <armadillo>

class HoppingParameter {
public:
    HoppingParameter(uint N, double J);

    ~HoppingParameter() = default;

    HoppingParameter(const HoppingParameter &p);

    HoppingParameter &operator=(const HoppingParameter &p);

    uint getN() const;

    double getJ() const;

    std::complex<double> get(uint i, uint j) const;

    arma::cx_mat asMatrix() const;

protected:
    HoppingParameter() = delete;

private:
    uint N;
    double J;
    boost::multi_array<std::complex<double>, 2> values;
};

#endif //HOPPING_PARAMETER_H
