#ifndef IMPURITY_MODEL_SYK4_H
#define IMPURITY_MODEL_SYK4_H

#include "HamiltonianTerm.h"
#include "InteractionParameter.h"

class SYK4 : public HamiltonianTerm {
public:
    SYK4(uint N, double paramJ);

    SYK4(const SYK4 &s);

    SYK4 &operator=(const SYK4 &s);

    ~SYK4() = default;

    void addEntries(TermCallback *callback, const Basis &basis, std::set<uint> bandSites) override;

    void update() override;

    std::string toString() const override;

    const InteractionParameter &getJ() const;

private:
    InteractionParameter J;
};


#endif //IMPURITY_MODEL_SYK4_H
