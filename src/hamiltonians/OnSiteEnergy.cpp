#include "OnSiteEnergy.h"
#include "../utils.h"
#include "../Log.h"
#include <iostream>

using namespace std;
using namespace arma;

OnSiteEnergy::OnSiteEnergy(const arma::vec &energies) : HamiltonianTerm(true), energies(energies) {
}

OnSiteEnergy::OnSiteEnergy(const OnSiteEnergy &o) : HamiltonianTerm(o) {
    this->operator=(o);
}

OnSiteEnergy &OnSiteEnergy::operator=(const OnSiteEnergy &o) {
    if (&o != this) {
        HamiltonianTerm::operator=(o);
        energies = o.energies;
    }

    return *this;
}

string OnSiteEnergy::toString() const {
    stringstream s;
    s << "on-site energy (" << Utils::convert<vec, double>(energies) << ")";
    return s.str();
}

const arma::vec &OnSiteEnergy::getEnergies() const {
    return energies;
}

void OnSiteEnergy::addEntries(TermCallback *callback, const Basis &basis, set<uint> bandSites) {
    const vector<State> &states = basis.getStates();
    ulong idx = 0;

    for (State state : states) {
        if (basis.getStates().size() > 100 && basis.getN() > 12 && (idx++) % 100 == 0)
            Log::debug() << "\tstate " << idx << " / " << states.size() << endl;

        int stateIdx = basis.getStateIndex(state);

        for (uint i : bandSites) {
            if (state.isSiteOccupied(i)) {
                callback->addEntry(energies(i), state, state, stateIdx, stateIdx);
            }
        }
    }
}
