#include "utils.h"
#include <boost/math/special_functions.hpp>
#include <armadillo>

using namespace std;
using namespace arma;

int Utils::binomial(int n, int k) {
    if (n < 0 || k < 0 || n < k)
        return 0;
    else
        return (int) boost::math::binomial_coefficient<double>(n, k);
}

double Utils::delta(double x, double eta) {
    return eta / ((x * x + eta * eta) * M_PI);
}

std::vector<double> Utils::linSpaced(uint num, double min, double max) {
    vec v = linspace(min, max, num);
    return convert<vec, double>(v);
}

std::vector<double> Utils::arange(double min, double max, double step) {
    vector<double> v;
    double d = min;
    while (d <= max) {
        v.push_back(d);
        d += step;
    }
    return v;
}

vector<uint> Utils::arange(uint min, uint max) {
    vector<uint> v;
    for (uint i = min; i < max; i++) {
        v.push_back(i);
    }
    return v;
}

vector<double> Utils::rescale(const vector<double> &vec, const vector<double> &factors) {
    vector<double> v(vec.size());
    for (uint i = 0; i < vec.size(); ++i) {
        v[i] = factors[i] * vec[i];
    }
    return v;
}

vector<string> Utils::split(string s, char delimiter) {
    vector<string> tokens;
    stringstream stream(s);

    string tmp;
    while (getline(stream, tmp, delimiter)) {
        tokens.push_back(tmp);
    }

    return tokens;
}