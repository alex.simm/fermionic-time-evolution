#include "Hamiltonian.h"
#include "utils.h"

using namespace std;
using namespace arma;

Hamiltonian::Hamiltonian(uint N, vector<HamiltonianTerm *> terms)
        : terms(terms), impurityIndex(-1), bandSites(Utils::toSet(Utils::arange(0, N))),
          impurityEnergy(numeric_limits<double>::quiet_NaN()), impurityHopping(numeric_limits<double>::quiet_NaN()) {
}

Hamiltonian::Hamiltonian(uint N, vector<HamiltonianTerm *> terms, double impurityEnergy, double V)
        : terms(terms), impurityIndex(N), bandSites(Utils::toSet(Utils::arange(0, N))), impurityEnergy(impurityEnergy),
          impurityHopping(V) {
}

Hamiltonian::Hamiltonian(const Hamiltonian &h) : bandSites(h.bandSites) {
    this->operator=(h);
}

Hamiltonian &Hamiltonian::operator=(const Hamiltonian &s) {
    if (&s != this) {
        terms = s.terms;
        impurityIndex = s.impurityIndex;
        bandSites = s.bandSites;
        impurityEnergy = s.impurityEnergy;
        impurityHopping = s.impurityHopping;
    }

    return *this;
}

vector<HamiltonianTerm *> Hamiltonian::getTerms() const {
    return terms;
}

bool Hamiltonian::hasImpurity() const {
    return impurityIndex >= 0;
}

double Hamiltonian::getImpurityEnergy() const {
    return impurityEnergy;
}

double Hamiltonian::getHoppingPotential() const {
    return impurityHopping;
}

int Hamiltonian::getImpurityIndex() const {
    return impurityIndex;
}

set<uint> Hamiltonian::getBandSites() const {
    return bandSites;
}

double Hamiltonian::partitionFunction(double beta) const {
    if (!isDiagonalised())
        throw runtime_error("HamiltonianBlock is not diagonalised");
    vec v = Utils::applyTo(getEigenvalues(), [beta](double e) { return exp(-beta * e); });
    return sum(v);
}

arma::vec Hamiltonian::boltzmannWeights(double beta) const {
    if (!isDiagonalised())
        throw runtime_error("HamiltonianBlock is not diagonalised");
    vec evals = getEigenvalues();
    vec diff = evals - min(evals);
    vec v = Utils::applyTo(diff, [beta](double e) { return exp(-beta * e); });
    double Z = sum(v);
    return v / Z;
}

double Hamiltonian::averageEnergy(double beta) const {
    double Z = partitionFunction(beta);

    vec v = Utils::applyTo(getEigenvalues(), [beta](double e) { return e * exp(-beta * e); });
    return sum(v) / Z;
}
