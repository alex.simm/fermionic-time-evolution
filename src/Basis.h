#ifndef IMPURITY_MODEL_BASIS_H
#define IMPURITY_MODEL_BASIS_H

#include "State.h"
#include <vector>
#include <map>
#include <set>

/**
 * Basis for a Hilbert or Fock space.
 */
class Basis {
public:
    /**
     * Creates a basis for N sites and a variable number of fermions.
     */
    Basis(uint N);

    /**
     * Creates a basis for N sites and Q fermions. If Q is negative the basis will contain all states with a variable
     * number of fermions.
     */
    Basis(uint N, int Q);

    Basis(const Basis &b);

    Basis &operator=(const Basis &b);

    ~Basis() = default;

    /** Returns the number of sites. */
    uint getN() const;

    /** Returns the number of fermions. */
    uint getQ() const;

    /** Returns the dimension of the Hilbert space. */
    ulong dim() const;

    std::set<uint> getSites() const;

    /** Returns a list of all states in the Hilbert space of this Hamiltonian. */
    const std::vector<State> &getStates() const;

    /** Returns the state with the given index in this model or throws an exception if no state with that index exists. */
    const State &getState(uint index) const;

    /** Returns the index of the state in this model or -1 if it does not exist. */
    int getStateIndex(const State &state) const;

private:
    /** Number of sites */
    uint N;
    /** Number of fermions, or -1 for a Fock space */
    int Q;
    /** Pure states */
    std::vector<State> states;
    /** Maps state numbers to indices in the states vector. */
    std::map<ulong, int> stateIndices;
};

#endif //IMPURITY_MODEL_BASIS_H
