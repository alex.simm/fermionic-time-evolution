## Directory structure
- `src`: main source files
    - `hamiltonians`: classes that represent terms in a Hamiltonian, like hopping, chemical potential, or random SYK interaction
    - `simulations`: implementations of exact and Lanczos time evolution
    - `observables`: classes that represent observables and that can be added to a simulation
    - `cli`: utility classes that parse command line options
    - `utils`: additional utility classes

- `test`: unit tests build on [Catch2](https://github.com/catchorg/Catch2)

## Compiling
Required libraries:
* armadillo (>= version 9.900.3)
* lapack (>= version 3.9.0)
* OpenBLAS (>= version 3.12)
* arpack (>= version 2.1.0)
* optionally OpenMP (libomp) for parallelisation

Building is done with CMake. The main CMake file `CMakeLists.txt` includes the list of classes from the
subdirectories. Since the project needs several libraries, a custom configuration might be necessary. For 
that the CMake file contains two variables `CUSTOM_INCLUDES` and `CUSTOM_LIBRARIES`, which specify where 
to find the header files and libraries. Directories and libraries can be added automatically using `find_package` 
or manually using `link_directories` and need to be added to the two variables.

## Usage of the classes
Typical procedure to measure something during time evolution:
- Choose several terms for the Hamiltonian from `src/hamiltonians`
- Create a `Hamiltonian` instance with these terms, either a `HamiltonianBlock` for a fixed fermion number or a `BlockDiagonalHamiltonian` that includes all blocks
- Create a `Simulation` with the Hamiltonian: either `ExactSimulation` or `LanczosSimulation`
- Initialise the simulation to any initial state via `Simulation::setPsi`
- Choose some observables that should be measured during time evolution from `src/observables`
- Run time evolution manually by
    - repeatedly calling `Simulation::evolve`
    - measuring the observables via `Observable::measure` using the state from `Simulation::getPsi`
- ...or let the time evolution run automatically by using `SimulationUtils::evolve`, which takes a list of observables and a callback function as parameters. In each time step the observables are measured and the callback is called.

### Example
```
uint N = 12; // number of band sites
uint Q = 6; // number of fermions, half filled
double J = 1; // interaction strength of the SYK term
double mu = 0.5; // chemical potential
uint nt = 1000; // number of time steps
double dt = 0.01; // time step size

// create terms for an SYK Hamiltonian with chemical potential
SYK4 sykTerm(N, J);
ChemicalPotential chemTerm(mu);

// create the Hamiltonian and diagonalise it
HamiltonianBlock H(N, Q, {&sykTerm, &chemTerm});
H.diagonalise();

// create observables
OccupationObservable occupation(H.getBasis());
EntropyObservable entropy(H.getBasis(), Utils::toSet(Utils::arange(0, N/2)));

// create a simulation and run
ExactSimulation sim(&H);
SimulationUtils::evolve(&sim, nt, dt, [](uint step, uint numSteps) {
    cout << "step " << step << " of " << numSteps << endl;
}, {&occupation, &entropy});

// do something will the measurements
vector<double> timestamps = Utils::linSpaced(nt, 0, nt * dt);
vector<double> occupationValues = occupation.getOccupations(0);
vector<double> entropyValues = entropy.getEntropy();
```